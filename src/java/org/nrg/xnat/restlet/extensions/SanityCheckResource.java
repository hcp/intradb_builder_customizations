package org.nrg.xnat.restlet.extensions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.IOUtils;
import org.nrg.action.ClientException;
import org.nrg.hcp.utils.CsvParser;
import org.nrg.hcp.utils.HCPScriptExecUtils;
import org.nrg.hcp.utils.ScriptResult;
import org.nrg.xdat.model.ValSanitychecksCheckI;
import org.nrg.xdat.model.ValSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.ValSanitychecksScanChecksI;
import org.nrg.xdat.om.ValSanitychecks;
import org.nrg.xdat.om.ValSanitychecksCheck;
import org.nrg.xdat.om.ValSanitychecksScanChecks;
import org.nrg.xdat.om.ValSanitychecksScanChecksCheck;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImageassessordata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;


@XnatRestlet({	
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SCAN_ACCEPTCHECKS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SCAN_ACCEPTCHECKS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SESSION_ACCEPTCHECKS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SESSION_ACCEPTCHECKS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.MARK_SCANS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.MARK_SCANS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.OVERALL_STATUS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.OVERALL_STATUS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CALCULATE_STATUS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CALCULATE_STATUS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.UPDATE_STATUS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.UPDATE_STATUS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CSV_UPDATE_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CSV_UPDATE_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.RUN_CHECKS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.RUN_CHECKS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.EMAIL_ON_RUN_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.EMAIL_ON_RUN_SEGMENT,
			})
public class SanityCheckResource extends SecureResource {
	
	private static final String CHECK_ID = "CheckShortName";
	private static final String CHECK_DESC = "CheckLongName";
	private static final String CHECK_STATUS = "Pass_Fail";
	private static final String CHECK_DIAG = "Description_of_Failure";
	
	public static final String[] csv_columns = { "SessionID","ScanNumber","SeriesDesc", CHECK_ID, CHECK_DESC, CHECK_STATUS, CHECK_DIAG };
	
	public static final String SCAN_ACCEPTCHECKS_SEGMENT = "acceptScanChecks";
	public static final String SESSION_ACCEPTCHECKS_SEGMENT = "acceptSessionChecks";
	public static final String MARK_SCANS_SEGMENT = "markScansUnusable";
	public static final String OVERALL_STATUS_SEGMENT = "getOverallStatus";
	public static final String CALCULATE_STATUS_SEGMENT = "calculateOverallStatus";
	public static final String UPDATE_STATUS_SEGMENT = "updateOverallStatus";
	public static final String CSV_UPDATE_SEGMENT = "updateViaCSV";
	public static final String RUN_CHECKS_SEGMENT = "runSanityChecks";
	public static final String EMAIL_ON_RUN_SEGMENT = "emailOnRun";
	
	private static final String FAILED_STATUS = "FAILED";
	private static final String ACCEPTED_STATUS = "ACCEPTED";
	private static final String PASSED_STATUS = "PASSED";
	private static final String UNUSABLE_STATUS = "SCAN_UNUSABLE";
	
	private static final String QUALITY_UNUSABLE = "unusable";
	private static final String SESSION_INDICATOR = "SESSION";
	private static final String SANITY_CHECK_LABEL_EXTENSION = "_sc";
	private XnatProjectdata proj = null;
	private XnatSubjectdata subj = null;
	private XnatExperimentdata exp = null;
	private ValSanitychecks sanityChecks = null;
	private XnatImagesessiondata imageSession = null;
	private static final List<String> sessionEmailList = new ArrayList<String>();

	private List<FileWriterWrapperI> fwlist;
	
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SanityCheckResource.class);

	public SanityCheckResource(Context context, Request request, Response response) {
		super(context, request, response);

		final String projID = (String)getParameter(request,"PROJECT_ID");
		final String subjID = (String)getParameter(request,"SUBJECT_ID");
		final String exptID = (String)getParameter(request,"EXP_ID");
		if (projID!=null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projID, user, false);
			if (proj == null) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Specified project either does not exist or user is not permitted access to subject data");
			}
			subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjID, user, false);
			if (subj==null) {
				subj = XnatSubjectdata.getXnatSubjectdatasById(subjID, user, false);
			}
			if (subj != null && (proj != null && !subj.hasProject(proj.getId()))) {
				subj = null;
			}
			if (subj == null) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Specified subject either does not exist or user is not permitted access to subject data");
			}
			exp = XnatExperimentdata.getXnatExperimentdatasById(exptID, user, false);
			if (exp != null && (proj != null && !exp.hasProject(proj.getId()))) {
				exp = null;
			}
			if(exp==null){
				exp = (XnatExperimentdata)XnatExperimentdata.GetExptByProjectIdentifier(proj.getId(),exptID, user, false);
			}
			
		} else {
			
			exp = XnatExperimentdata.getXnatExperimentdatasById(exptID, user, false);
			proj = exp!=null ? exp.getProjectData() : null;
			
		}
		if (exp==null) {
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"ERROR:  Could not locate specified experiment");
			return;
		}
		try {
			if (!proj.canEdit(user)) { 
				response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED,"User account is not permitted to access this service");
				return;
			}
		} catch (Exception e) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"INTERNAL ERROR:  Could not evaluate user permissions");
			return;
		}
		if (exp instanceof ValSanitychecks) {
			sanityChecks = (ValSanitychecks)exp;
			imageSession = sanityChecks.getImageSessionData();
		} else if (exp instanceof XnatImagesessiondata) {
			imageSession = (XnatImagesessiondata)exp;
		}
	}

	@Override
	public boolean allowGet() {
		return true;
	}

	@Override
	public void handleGet() {
		final List<String> segments = getRequest().getResourceRef().getSegments();
		if (segmentCount(segments)>1) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Invalid URL");
			return;
		}
		if (segments.contains(OVERALL_STATUS_SEGMENT)) {
			getOverallStatus();
		} else if (segments.contains(CALCULATE_STATUS_SEGMENT)) {
			calculateOverallStatus(true);
		}
	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {
		final String[] scanA = hasQueryVariable("scanList") ? getQueryVariable("scanList").toString().split(",") : new String[]{};
		final List<String> segments = getRequest().getResourceRef().getSegments();
		if (segmentCount(segments) != 1) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Invalid URL");
			return;
		}
		if (segments.contains(SCAN_ACCEPTCHECKS_SEGMENT)) {
			if (validateType(ValSanitychecks.class)) {
				acceptScanChecks(scanA);
			}
		} else if (segments.contains(SESSION_ACCEPTCHECKS_SEGMENT)) {
			if (validateType(ValSanitychecks.class)) {
				acceptSessionChecks();
			}
		} else if (segments.contains(MARK_SCANS_SEGMENT)) {
			if (validateType(ValSanitychecks.class)) {
				markScansUnusable(scanA);
			}
		} else if (segments.contains(UPDATE_STATUS_SEGMENT)) {
			if (validateType(ValSanitychecks.class)) {
				updateOverallStatusDoSave();
			}
		} else if (segments.contains(CSV_UPDATE_SEGMENT)) {
			if (validateType(XnatMrsessiondata.class)) {
				processUploadedFile();
			}
		} else if (segments.contains(EMAIL_ON_RUN_SEGMENT)) {
			if (!sessionEmailList.contains(imageSession.getId())) {
				sessionEmailList.add(imageSession.getId());
			}
			getResponse().setStatus(Status.SUCCESS_OK);
		} else if (segments.contains(RUN_CHECKS_SEGMENT)) {
			final ScriptResult result;
			if (exp.getClass().isAssignableFrom(XnatMrsessiondata.class)) {
				result = HCPScriptExecUtils.runSanityChecks(user, proj, (XnatMrsessiondata) exp, false);
				setStatusBasedOnScriptResult(result);
				sendUserEmail();
			} else if (exp.getClass().isAssignableFrom(ValSanitychecks.class)) {
				result = HCPScriptExecUtils.runSanityChecks(user, proj, (XnatMrsessiondata) imageSession, false);
				setStatusBasedOnScriptResult(result);
				sendUserEmail();
			} 
		}
	}
	
	
	private void sendUserEmail() {
		List<String> junkList = sessionEmailList;
		if (sessionEmailList.contains(imageSession.getId())) {
			sessionEmailList.remove(imageSession.getId());
			final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
  	      	cc.addClause("val:SanityChecks.imageSession_ID",imageSession.getId());
  	      	cc.addClause("val:SanityChecks.project",proj.getId());
			final ArrayList<ValSanitychecks> schecks = ValSanitychecks.getValSanitycheckssByField(cc, user, false);
			if (schecks != null && schecks.size()>0) {
				try {
					final ValSanitychecks scheck = schecks.get(0);
					final StringBuilder returnSB=new StringBuilder();
						returnSB.append("<h3>Sanity Check Status:  ")
							.append(scheck.getOverall_status())
							.append("</h3>") 
							.append("<h3>MR Session:  <a href=\"") 
							.append(TurbineUtils.GetFullServerPath()) 
							.append("/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/") 
							.append(imageSession.getId()) 
							.append("/project/") 
							.append(proj.getId()) 
							.append("\">") 
							.append(imageSession.getLabel()) 
							.append("</a></h3>\n") 
							.append("<h3>Sanity Check Session:  <a href=\"") 
							.append(TurbineUtils.GetFullServerPath()) 
							.append("/app/action/DisplayItemAction/search_element/val:sanityChecks/search_field/val:sanityChecks.ID/search_value/") 
							.append(scheck.getId()) 
							.append("/project/") 
							.append(proj.getId()) 
							.append("\">") 
							.append(scheck.getLabel()) 
							.append("</a></h3>\n");
						/*
					final String returnString = 
						"<h3>Sanity Check Status:  " + scheck.getOverall_status() + "</h3>" + 
			  		    "<h3>MR Session:  <a href=\"" + TurbineUtils.GetFullServerPath() +
       		                 "/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/" +
         	               imageSession.getId() + "/project/" + proj.getId() + "\">" + imageSession.getLabel() + "</a></h3>\n" +
          		        "<h3>Sanity Check Session:  <a href=\"" + TurbineUtils.GetFullServerPath() + 
            	            "/app/action/DisplayItemAction/search_element/val:sanityChecks/search_field/val:sanityChecks.ID/search_value/" +
						*/
					AdminUtils.sendUserHTMLEmail("Sanity Check run is complete (SESSION=" + exp.getLabel() + ")", returnSB.toString(), false, new String[] { user.getEmail() });
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void setStatusBasedOnScriptResult(ScriptResult result) {
		if (result.getStatus()) {
			getResponse().setStatus(Status.SUCCESS_OK, "Sanity checks updated");
		} else {
			if (result.getReturnCode() != null) {
				if (result.getReturnCode().intValue() == 98) {
					getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"ERROR:  Sanity check script is already running for this subject/experiment.  Duplicate invocations are not allowed");
				} else {
					getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"PROCESSING ERROR RETURN CODE=" + result.getReturnCode() + ":  " + result.getResultList());
				}
			} else {
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"PROCESSING ERROR:  " + result.getResultList());
			}
		}
	}

	private int segmentCount(List<String> segments) {
		return bool2int(segments.contains(SCAN_ACCEPTCHECKS_SEGMENT))+ bool2int(segments.contains(SESSION_ACCEPTCHECKS_SEGMENT))+
				bool2int(segments.contains(MARK_SCANS_SEGMENT))+bool2int(segments.contains(OVERALL_STATUS_SEGMENT))+
				bool2int(segments.contains(CALCULATE_STATUS_SEGMENT))+
				bool2int(segments.contains(UPDATE_STATUS_SEGMENT))+bool2int(segments.contains(CSV_UPDATE_SEGMENT))+
				bool2int(segments.contains(RUN_CHECKS_SEGMENT))+
				bool2int(segments.contains(EMAIL_ON_RUN_SEGMENT))
				;
	}

	private int bool2int(boolean b) {
		return b ? 1 : 0;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private boolean validateType(Class c) {
		if (!(exp.getClass().isAssignableFrom(c))) {
			final StringBuilder rMsg = new StringBuilder("ERROR:  Experiment is wrong type for this URL");
			if (c.isAssignableFrom(XnatMrsessiondata.class)) {
				rMsg.append(" (Should be xnat:mrSessiondata)");
			} else if (c.isAssignableFrom(ValSanitychecks.class)) {
				rMsg.append(" (Should be val:sanityChecks)");
			} 
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,rMsg.toString());
			return false;
		}
		return true;
	}
	
	private void processUploadedFile() {

		final Representation entity = getRequest().getEntity();
		try {
			fwlist=this.getFileWritersAndLoadParams(entity,false);
		} catch (ClientException e1) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"SERVER ERROR:  Unable to process uploaded file.");
			return;
		} catch (FileUploadException e1) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"SERVER ERROR:  Unable to process uploaded file.");
			return;
		}
		if (fwlist.size() == 0) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  No CSV file received.");
			return;
		} else if ( fwlist.size() > 1) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  May upload only one file per request.");
			return;
		} 
		final FileWriterWrapperI fw = fwlist.get(0);


		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		final Date d = Calendar.getInstance().getTime();
		final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
		final String uploadID = formatter.format(d);
			
		// Save input file to cache space
		cachePath+="user_uploads/"+user.getXdatUserId() + "/" + uploadID + "/";
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
			
		final String fileName = fw.getName();
		//String writeout = null;
		final File cacheFile = new File(cacheLoc,fileName);
		try {
			//StringWriter writer = new StringWriter();
			final FileWriter writer = new FileWriter(cacheFile);
			IOUtils.copy(fw.getInputStream(), writer);
			writer.close();
		} catch (IOException e) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"SERVER ERROR:  Unable to process CSV file.");
			return;
		}
		
		processCSV(cacheFile);
		
	}

	private void processCSV(File cacheFile) {
		
		final List<Map<String,String>> csvRep;
		try {
			csvRep = CsvParser.parseCSV(cacheFile);
		} catch (IOException e) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Unable to parse CSV file.  Please check fild and verify it's in CSV format.");
			return;
		}
		if (csvRep==null || csvRep.size()<1) {
			// Let's go ahead and process empty CSV files so an empty data type record can be generated
			//getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Uploaded file contains no data");
			//return;
		} else if (!validateCSVFile(csvRep)) {
			return;
		}
		
		final Map<String,List<Map<String,String>>> scanUpdates = separateIntoScans(csvRep);
		//ValSanitychecks checks = ValSanitychecks.
		final ArrayList<XnatImageassessordata> assessors = imageSession.getAssessors(ValSanitychecks.SCHEMA_ELEMENT_NAME);
		if (assessors.isEmpty()) {
			if (createNewSanityChecksAssesorAndUpdate(scanUpdates)) {
				getResponse().setStatus(Status.SUCCESS_OK,"New sanity check assessor has been created");
			} 
		} else if (assessors.size()==1) {
			if (updateExistingSanityChecksAssessor((ValSanitychecks)assessors.get(0),scanUpdates)) {
				getResponse().setStatus(Status.SUCCESS_OK,"Sanity check assessor for experiment has been updated");
			} 
		} else if (assessors.size()>1) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Image sessions contains multiple sanity checks assessors.  Should only have one.");
			return;
		}
	
	}

	private boolean validateCSVFile(List<Map<String, String>> csvRep) {
		final Set<String> keys = csvRep.get(0).keySet();
		for (String col : Arrays.asList(csv_columns)) {
			if (!(keys.contains(col))) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"ERROR:  CSV file doesn't contain expected columns (" + csv_columns.toString() + ")");
				return false;
			}
		}
		for (Map<String,String> rowmap : csvRep) {
			final String sessionID  = rowmap.get("SessionID");
			final String scanID  = rowmap.get("ScanNumber");
			if (sessionID == null || !sessionID.equals(imageSession.getLabel())) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"ERROR:  CSV file should contain data for and ONLY contain data for current session (" + imageSession.getLabel() + ")" );
				return false;
			}
			if (scanID!=null && scanID.trim().length()>0 && imageSession.getScanById(scanID)==null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"ERROR:  CSV file contains data for scans not currently in the session (" + scanID + ")" );
				return false;
			}
		}
		return true;
	}

	private boolean createNewSanityChecksAssesorAndUpdate(Map<String, List<Map<String, String>>> scanUpdates) {
		
		try {
		
			final ValSanitychecks schecks = new ValSanitychecks((UserI)user);
			schecks.setId(XnatExperimentdata.CreateNewID());
			schecks.setDate(new Date());
			schecks.setLabel(imageSession.getLabel() + SANITY_CHECK_LABEL_EXTENSION);
			schecks.setImagesessionId(imageSession.getId());
			schecks.setProject(imageSession.getProject());
			imageSession.addAssessors_assessor(schecks);
			applyUpdates(schecks,scanUpdates);
			saveSanityCheckEntry(schecks);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Exception encountered creating new sanity check assessor (" + e.toString() + ").");
			return false;
		}
		
	}

	private boolean updateExistingSanityChecksAssessor(ValSanitychecks schecks,Map<String, List<Map<String, String>>> scanUpdates) {
			
		try {
			
			applyUpdates(schecks,scanUpdates);
			// Per Greg, sanity checks date should update every time record is checked (reflect the last time was checked).
			schecks.setDate(new Date());
			saveSanityCheckEntry(schecks);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Exception encountered updating sanity check assessor (" + e.toString() + ").");
			return false;
		}
			
	}
	
	private void applyUpdates(ValSanitychecks schecks, Map<String, List<Map<String, String>>> scanUpdates) throws Exception {
		
		if (!scanUpdates.containsKey(SESSION_INDICATOR) && schecks.getSession_status() == null) {
			// Set session status to passed if there are no session level checks
			schecks.setSession_status(PASSED_STATUS);
			
		}
		for (final String key : scanUpdates.keySet()) {
			Boolean newFailed = false;
			final List<Map<String, String>> csvrows = scanUpdates.get(key);
			final List<String> currentIDS = new ArrayList<String>();
			for (Map<String,String> csvrow : csvrows) {
				currentIDS.add(csvrow.get(CHECK_ID));
			}	
			for (Map<String,String> csvrow : csvrows) {
				final String checkScanID=key;
				final String checkID=csvrow.get(CHECK_ID);
				final String checkDesc=csvrow.get(CHECK_DESC);
				final String checkStatus=convertCheckStatus(csvrow.get(CHECK_STATUS));
				final String checkDiag=csvrow.get(CHECK_DIAG);
				
				if (checkScanID.equalsIgnoreCase(SESSION_INDICATOR)) {
					boolean matchingCheck = false;
					for (final ValSanitychecksCheckI check : schecks.getSession_sessionChecks_check()) {
						if (check.getId().equals(checkID)) {
							matchingCheck = true;
							updateStatus(check,checkStatus,checkDiag,newFailed);
							if (checkDesc != null && checkDesc.length()>0) {
								check.setDescription(checkDesc);
							}
						}
					}
					if (!matchingCheck) {
						final ValSanitychecksCheckI check = new ValSanitychecksCheck((UserI)user);
						check.setId(checkID);
						check.setStatus(checkStatus);
						check.setDescription(checkDesc);
						if (check.getStatus().equalsIgnoreCase(FAILED_STATUS)) {
							newFailed = true;
							check.setDiagnosis(checkDiag);
						}
						schecks.addSession_sessionChecks_check(check);
					}
				} else {	
					boolean matchingCheck = false;
					ValSanitychecksScanChecksI scan = getChecksForScan(schecks,checkScanID);
					if (scan != null) {
						for (final ValSanitychecksScanChecksCheckI check : scan.getCheck()) {
							if (check.getId().equals(checkID)) {
								matchingCheck = true;
								updateStatus(check,checkStatus,checkDiag,newFailed);
								if (checkDesc != null && checkDesc.length()>0) {
									check.setDescription(checkDesc);
								}
							}
						}
					} else {
						scan =  new ValSanitychecksScanChecks();
						scan.setScanId(checkScanID);
						schecks.addScans_scanChecks(scan);
					}
					if (!matchingCheck) {
						final ValSanitychecksScanChecksCheckI check = new ValSanitychecksScanChecksCheck((UserI)user);
						check.setId(checkID);
						check.setStatus(checkStatus);
						check.setDescription(checkDesc);
						if (check.getStatus().equalsIgnoreCase(FAILED_STATUS)) {
							newFailed = true;
							check.setDiagnosis(checkDiag);
						}
						scan.addCheck(check);
					}
				}
			}
		}
		updateSessionAndScanAndOverallStatus(schecks);
	}

	private void updateSessionAndScanAndOverallStatus(ValSanitychecks schecks) {
		schecks.setSession_status(PASSED_STATUS);
		boolean anyFailed = false;
		boolean anyAccepted = false;
		for (final ValSanitychecksCheckI check : schecks.getSession_sessionChecks_check()) {
			if (schecks.getSession_status().equals(FAILED_STATUS)) {
				continue;
			}
			if (check.getStatus().equals(FAILED_STATUS)) {
				anyFailed = true;
				schecks.setSession_status(FAILED_STATUS);
			} else if (check.getStatus().equals(ACCEPTED_STATUS) && schecks.getSession_status().equals(PASSED_STATUS)) {
				anyAccepted = true;
				schecks.setSession_status(ACCEPTED_STATUS);
			}
		}
		final List<ValSanitychecksScanChecksI> scans = schecks.getScans_scanChecks();
		for (ValSanitychecksScanChecksI scan : scans) {
			final XnatImagescandata mrScan = imageSession.getScanById(scan.getScanId());
			if (mrScan.getQuality() != null && mrScan.getQuality().equalsIgnoreCase(QUALITY_UNUSABLE)) {
				// Only set status here (required for SQL constraints).  Don't want to include in overall status and if usability changes,
				// we want indicator in place that the scan was unusable when sanity checks were run.
				scan.setStatus(UNUSABLE_STATUS);
				continue;
			}
			scan.setStatus(PASSED_STATUS);
			for (final ValSanitychecksScanChecksCheckI check : scan.getCheck()) {
				if (scan.getStatus().equals(FAILED_STATUS)) {
					continue;
				}
				if (check.getStatus().equals(FAILED_STATUS)) {
					anyFailed = true;
					scan.setStatus(FAILED_STATUS);
				} else if (check.getStatus().equals(ACCEPTED_STATUS) && scan.getStatus().equals(PASSED_STATUS)) {
					anyAccepted = true;
					scan.setStatus(ACCEPTED_STATUS);
				}
			}
		}
		if (anyFailed) {
			schecks.setOverall_status(FAILED_STATUS);
		} else if (anyAccepted) {
			schecks.setOverall_status(ACCEPTED_STATUS);
		} else {
			schecks.setOverall_status(PASSED_STATUS);
		}
	}

	private ValSanitychecksScanChecksI getChecksForScan(ValSanitychecks schecks, String checkScanID) {
		final List<ValSanitychecksScanChecksI> scans = schecks.getScans_scanChecks();
		for (ValSanitychecksScanChecksI scan : scans) {
			if (scan.getScanId().equalsIgnoreCase(checkScanID)) {
				return scan;
			}
		}
		return null;
	}

	private void updateStatus(ValSanitychecksCheckI check, String checkStatus, String checkDiag, Boolean newFailed) {
		
		if (checkStatus.equals(PASSED_STATUS)) {
			check.setStatus(PASSED_STATUS);
			check.setDiagnosis(null);
		} else if (checkStatus.equals(FAILED_STATUS)) {
			check.setDiagnosis(checkDiag);
			if (check.getStatus().equals(PASSED_STATUS)) {
				newFailed = true;
			}
			// TO DO: Should we compare diagnoses here?????
			if (!check.getStatus().equals(ACCEPTED_STATUS)) {
				check.setStatus(FAILED_STATUS);
			}
		} else if (checkStatus.equals(ACCEPTED_STATUS)) {
			if (checkStatus.equals(PASSED_STATUS)) {
				check.setStatus(PASSED_STATUS);
				check.setDiagnosis(null);
			}
		}
		
	}
	
	private void updateStatus(ValSanitychecksScanChecksCheckI check, String checkStatus, String checkDiag, Boolean newFailed) {
		
		if (checkStatus.equals(PASSED_STATUS)) {
			check.setStatus(PASSED_STATUS);
			check.setDiagnosis(null);
		} else if (checkStatus.equals(FAILED_STATUS)) {
			check.setDiagnosis(checkDiag);
			if (check.getStatus().equals(PASSED_STATUS)) {
				newFailed= true;
			}
			// TO DO: Should we compare diagnoses here?????
			if (!check.getStatus().equals(ACCEPTED_STATUS)) {
				check.setStatus(FAILED_STATUS);
			}
		} else if (checkStatus.equals(ACCEPTED_STATUS)) {
			if (checkStatus.equals(PASSED_STATUS)) {
				check.setStatus(PASSED_STATUS);
				check.setDiagnosis(null);
			}
		}
		
	}

	private String convertCheckStatus(String ins) {
		if (ins.toUpperCase().startsWith("PASS")) {
			return PASSED_STATUS;
		} else if (ins.toUpperCase().startsWith("FAIL")) {
			return FAILED_STATUS;
		}
		return ins;
	}

	private void saveSanityCheckEntry(ValSanitychecks schecks) throws Exception {
		
		final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, schecks.getItem(),
				EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
		final EventMetaI ci = wrk.buildEvent();
		if (SaveItemHelper.authorizedSave(schecks,user,false,true,ci)) {
			PersistentWorkflowUtils.complete(wrk, ci);
		} else {
			PersistentWorkflowUtils.fail(wrk,ci);
		}
		
	}

	private Map<String,List<Map<String, String>>> separateIntoScans(List<Map<String, String>> csvRep) {
		final HashMap <String,List<Map<String,String>>> scanMap = new HashMap<String,List<Map<String,String>>>();
		for (Map<String,String> rowmap : csvRep) {
			String scanNumber = rowmap.get("ScanNumber");
			if (scanNumber == null || scanNumber.trim().length()<1) {
				// Session checks have blank scan number
				scanNumber=SESSION_INDICATOR;
			}
			if (!scanMap.containsKey(scanNumber)) {
				final ArrayList <Map<String,String>> scanChecks = new ArrayList<Map<String,String>>();
				scanMap.put(scanNumber, scanChecks);
			}
			scanMap.get(scanNumber).add(rowmap);
		}
		return scanMap;
	}

	private void getOverallStatus() {
		this.getResponse().setEntity(new StringRepresentation(sanityChecks.getOverall_status(),MediaType.TEXT_PLAIN));
	}

	private String calculateOverallStatus(boolean setResponse) {
		boolean hasAccepted=false, hasFailed=false;
		final String rtnVal;
		if (sanityChecks.getSession_status().equalsIgnoreCase(FAILED_STATUS)) {
			hasFailed=true;
		} else if (sanityChecks.getSession_status().equalsIgnoreCase(ACCEPTED_STATUS)) {
			hasAccepted=true;
		}
		final List<ValSanitychecksScanChecksI> scanChecks = sanityChecks.getScans_scanChecks();
		for (ValSanitychecksScanChecksI sc : scanChecks) {
			if (sc.getStatus().equalsIgnoreCase(FAILED_STATUS)) {
				if (!getScanQuality(sc.getScanId()).equalsIgnoreCase(QUALITY_UNUSABLE)) {
					hasFailed=true;
				}
			} else if (sc.getStatus().equalsIgnoreCase(ACCEPTED_STATUS)) {
				if (!getScanQuality(sc.getScanId()).equalsIgnoreCase(QUALITY_UNUSABLE)) {
					hasAccepted=true;
				}
			}
		}
		if (hasFailed) {
			rtnVal = FAILED_STATUS;
		} else if (hasAccepted) {
			rtnVal = ACCEPTED_STATUS;
		} else {
			rtnVal = PASSED_STATUS;
		}
		if (setResponse) {
			this.getResponse().setEntity(new StringRepresentation(rtnVal,MediaType.TEXT_PLAIN));
		}
		return rtnVal;
	}
	
	private String getScanQuality(String scanId) {
		final XnatImagescandata scan = imageSession.getScanById(scanId);
		if (scan!=null) {
			final String quality = scan.getQuality();
			if (quality!=null) {
				return quality;
			}
		}
		return "";
	}

	private void updateOverallStatusDoSave() {
		updateOverallStatus();
		saveChanges(sanityChecks);
	}
	
	private void updateOverallStatus() {
		sanityChecks.setOverall_status(calculateOverallStatus(false));
	}
	
	@SuppressWarnings("deprecation")
	public void acceptScanChecks(String[] scanA) {
		if (!hasQueryVariable("scanList")) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Must specify one or more scans (scanList query variable).");
			return;
		}
		final List<ValSanitychecksScanChecksI> scanChecks = sanityChecks.getScans_scanChecks();
		final List<ValSanitychecksScanChecksI> modScanChecks = validateScanListAndReturnTBMList(scanA,scanChecks);
		if (modScanChecks==null) {
			return;
		}
		final Date modTime = new Date();
		for (ValSanitychecksScanChecksI sc : modScanChecks) {
			sc.setStatus(ACCEPTED_STATUS);
			List<ValSanitychecksScanChecksCheckI> checks = sc.getCheck();
			// Keep track of which checks have been accepted
			for (ValSanitychecksScanChecksCheckI check : checks) {
				if (check.getStatus().equals(FAILED_STATUS)) {
					check.setStatus(ACCEPTED_STATUS);
				}
			}
			sc.setStatusoverride_username(user.getLogin());
			sc.setStatusoverride_datetime(modTime);
			if (hasQueryVariable("justification")) {
				try {
					sc.setStatusoverride_justification(URLDecoder.decode(getQueryVariable("justification"),"UTF-8"));
				} catch (UnsupportedEncodingException e) {
					sc.setStatusoverride_justification(URLDecoder.decode(getQueryVariable("justification")));
				}
			}
		}
		saveChanges(sanityChecks);
	}
	
	@SuppressWarnings("deprecation")
	public void acceptSessionChecks() {
		final String sessionStatus = sanityChecks.getSession_status();
		if (sessionStatus==null || !sessionStatus.equalsIgnoreCase(FAILED_STATUS)) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Session-level current status is not " + FAILED_STATUS + ".");
			return;
			
		}
		final Date modTime = new Date();
		sanityChecks.setSession_status(ACCEPTED_STATUS);
		// Keep track of which checks have been accepted
		List<ValSanitychecksCheckI> checks = sanityChecks.getSession_sessionChecks_check();
		for (ValSanitychecksCheckI check : checks) {
			if (check.getStatus().equals(FAILED_STATUS)) {
				check.setStatus(ACCEPTED_STATUS);
			}
		}
		sanityChecks.setSession_statusoverride_username(user.getLogin());
		sanityChecks.setSession_statusoverride_datetime(modTime);
		if (hasQueryVariable("justification")) {
			sanityChecks.setSession_statusoverride_justification(getQueryVariable("justification"));
			try {
				sanityChecks.setSession_statusoverride_justification(URLDecoder.decode(getQueryVariable("justification"),"UTF-8"));
			} catch (UnsupportedEncodingException e) {
				sanityChecks.setSession_statusoverride_justification(URLDecoder.decode(getQueryVariable("justification")));
			}
		}
		saveChanges(sanityChecks);
	}
	
	private void saveChanges(XnatExperimentdata exp) {
		if (exp instanceof ValSanitychecks) {
			updateOverallStatus();
		} else {
			updateOverallStatusDoSave();
		}
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(exp,user,false,true,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
				getResponse().setStatus(Status.SUCCESS_OK);
				return;
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
		} catch (Exception e) {
			// Do nothing for now
		}
		getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Could not save changes");
	}

	private List<ValSanitychecksScanChecksI> validateScanListAndReturnTBMList(String[] scanA, List<ValSanitychecksScanChecksI> scanChecks) {
		final List<ValSanitychecksScanChecksI> modList = new ArrayList<ValSanitychecksScanChecksI>();
		for (final String scanID : scanA) {
			final ValSanitychecksScanChecksI sc = getScanChecks(scanChecks,scanID);
			if (sc==null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans do not exist in assessor.");
				return null;
			}
			if (sc.getStatus()==null || !sc.getStatus().equalsIgnoreCase(FAILED_STATUS)) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans current status is not " + FAILED_STATUS + ".");
				return null;
			}
			modList.add(sc);
		}
		return modList;
	}

	private List<XnatImagescandata> validateImageScanListAndReturnTBMList(String[] scanA, List<XnatImagescandata> scanChecks) {
		final List<XnatImagescandata> modList = new ArrayList<XnatImagescandata>();
		for (final String scanID : scanA) {
			final XnatImagescandata sc = imageSession.getScanById(scanID);
			if (sc==null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans do not exist in session.");
				return null;
			}
			if (sc.getQuality()==null || sc.getQuality().equalsIgnoreCase(QUALITY_UNUSABLE)) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans current quality already " + QUALITY_UNUSABLE + ".");
				return null;
			}
			modList.add(sc);
		}
		return modList;
	}

	private ValSanitychecksScanChecksI getScanChecks(List<ValSanitychecksScanChecksI> scanChecks, String scanID) {
		for (final ValSanitychecksScanChecksI sc : scanChecks) {
			if (sc.getScanId().equals(scanID)) {
				return sc;
			}
		}
		return null;
	}

	public void markScansUnusable(String[] scanA) {
		if (!hasQueryVariable("scanList")) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Must specify one or more scans (scanList query variable).");
			return;
		}
		final List<XnatImagescandata> scans = imageSession.getScans_scan();
		final List<XnatImagescandata> modScans = validateImageScanListAndReturnTBMList(scanA,scans);
		if (modScans==null) {
			return;
		}
		for (XnatImagescandata sc : modScans) {
			final ValSanitychecksScanChecksI scanCheck = getScanChecks(sanityChecks.getScans_scanChecks(),sc.getId());
			if (scanCheck==null) {
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Could not determine scan status in assessor");
				return;
			}
			if (!scanCheck.getStatus().equalsIgnoreCase(FAILED_STATUS)) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  May only set usability for scans marked " + FAILED_STATUS + " in sanity checks assessor.");
				return;
			}
			sc.setQuality(QUALITY_UNUSABLE);
			if (sc.getNote()==null || !sc.getNote().contains("SanityChecks")) {
				sc.setNote("Set to unusable by " + user.getLogin() + " via SanityChecks interface.");
			}
		}
		saveChanges(imageSession);
	}

}

	
