package org.nrg.hcp.utils.projectutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.nrg.hcp.utils.ReleaseRulesI;
import org.nrg.hcp.utils.ReleaseRulesException;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectassessordata;

import com.google.common.collect.Lists;

public class ReleaseRules_Phase2_7T implements ReleaseRulesI {
	
	public static final String  LOCALIZER_RGX = "^Localizer.*", 
								SCOUT_RGX = "^AAHScout.*",
								SCOUT_BIAS_RGX = "^AAHScout_32ch$",
								EJA_RGX = "^eja_ep2d_.*",
								BIAS_RGX = "(?i)^BIAS_.*", 
								FIELDMAP_RGX = "^FieldMap.*",
								ANYFIELDMAP_RGX = "(?i)^.*FieldMap.*",
								GEFIELDMAP_RGX = "(?i)^FieldMap_((Ma)|(Ph)).*",
								SEFIELDMAP_RGX = "^FieldMap_SE_.*",
								SPINECHO_RGX = "(?i)^SpinEchoFieldMap.*",
								T1_RGX = "(?i)^T1w.*",
								T2_RGX = "(?i)^T2w.*", 
								T1MPR_RGX = "(?i)^T1w_MPR[0-9]*.*$",
								T2SPC_RGX = "(?i)^T2w_SPC[0-9]*.*$",
								TFMRI_RGX = "(?i)^tfMRI.*", 
								RFMRI_RGX = "(?i)^rfMRI.*", 
								DWI_RGX = "(?i)^DWI.*", 
								DWIEXCLUDED_RGX = "(?i)^DWI.*((_SSref)|(_SEGref)).*$", 
								DMRI_RGX = "(?i)^dMRI.*", 
								BOLD_RGX = "(?i)^BOLD.*", 
								BOLDONLY_RGX = "(?i)^BOLD", 
								SBREF_RGX = "(?i)^.*_SBRef$", 
								AP_RGX = "(?i)^.*_AP(((_dir[0-9][0-9])?_SBRef)|(_SE)|(_dir[0-9][0-9]))?$", 
								PA_RGX = "(?i)^.*_PA(((_dir[0-9][0-9])?_SBRef)|(_SE)|(_dir[0-9][0-9]))?$", 
								APPA_RGX = "(?i)_((PA)|(AP))(($)|(_.*$))", 
								APPAONLY_RGX = "(?i)_((AP)|(PA))_", 
								TFMRINO_RGX = "(?i)[0-9]_((AP)|(PA)).*$", 
								MAINSCAN_RGX = "(?i)((T[12]w)|(tfMRI)|(rfMRI)|(dMRI))",
								MAINSCANDESC_RGX = "(?i)^((T[12]w)|(tfMRI)|(rfMRI)|(DWI)).*$",
								OTHER_RGX = "(?i)^.*MPR.*Collection.*$"
								;
	
	public static final String[] validQualityRatings = {"undetermined","usable","unusable","excellent","good","fair","poor"};
	public static final String[] structuralQualityRatings = {"excellent","good","fair","poor"};
	
	private final List<String> errorList = Lists.newArrayList();
	private final List<String> warningList = Lists.newArrayList();
	private boolean errorOverride;

	@Override
	public List<String> applyRulesToScans(
			SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans,
			String selectionCriteria, boolean errorOverride)
			throws ReleaseRulesException {
		// NOTE:  Currently Phase2_7T does not use the selectionCriteria parameter.  All sessions are combined into a single 7T session.
		return applyRulesToScans(subjectScans,errorOverride);
	}

	@Override
	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta,
			SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
			String selectionCriteria, HcpToolboxdata tboxexpt, NtScores ntexpt,
			HcpvisitHcpvisitdata visitexpt, boolean errorOverride)
			throws ReleaseRulesException {
		// NOTE:  Currently Phase2_7T does not use the selectionCriteria parameter.  All sessions are combined into a single 7T session.
		applyRulesToSubjectMetaData(subjMeta,scanMap,tboxexpt,ntexpt,visitexpt,errorOverride);
		
	}

	@Override
	public ArrayList<XnatSubjectassessordata> filterExptList(ArrayList<XnatSubjectassessordata> projectExpts, String selectionCriteria) throws ReleaseRulesException {
		// NOTE:  Currently Phase2_7T does not use the selectionCriteria parameter.  All sessions are combined into a single 7T session.
		return projectExpts;
	}

	public List<String> applyRulesToScans(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans, boolean errorOverride) throws ReleaseRulesException {
		
		// NOTE:  Order of these calls is VERY important
		this.errorOverride = errorOverride;
		nullPriorValues(subjectScans);
		setCountScanAndSession(subjectScans);
		setTargetForReleaseAndScanID(subjectScans);
		setTypeDescriptionAndOthers(subjectScans);
		setGroupValues(subjectScans);
		updaterfMRIDescAndFlagError(subjectScans);
		excludeUnneededBiasScans(subjectScans);
		rearrangeDWIDesc(subjectScans);
		setScanOrderAndScanComplete(subjectScans);
		flagDwiError(subjectScans);
		flagTaskError(subjectScans);
		hardCodedValues(subjectScans);
		fixBackslashProblem(subjectScans);
		if (!errorOverride && !errorList.isEmpty()) {
			throw new ReleaseRulesException(errorList.toString());
		}
		return warningList;
	}
	
	private List<String> getErrorOrWarningList() {
		return (errorOverride) ? warningList : errorList;
	}

	private void fixBackslashProblem(
			SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (XnatMrscandata scan : subjectScans.keySet()) {
			// Replace multiple backslash instances with a single backslash
			if (scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains("\\\\")) {
				String imageType = scan.getParameters_imagetype().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_imagetype(imageType);
			}
			if (scan.getParameters_seqvariant()!=null && scan.getParameters_seqvariant().contains("\\\\")) {
				String seqVariant = scan.getParameters_seqvariant().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_seqvariant(seqVariant);
			}
			if (scan.getParameters_scansequence()!=null && scan.getParameters_scansequence().contains("\\\\")) {
				String scanSequence = scan.getParameters_scansequence().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scansequence(scanSequence);
			}
			if (scan.getParameters_scanoptions()!=null && scan.getParameters_scanoptions().contains("\\\\")) {
				String scanOptions = scan.getParameters_scanoptions().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scanoptions(scanOptions);
			}
		}
	}

	public List<String> applyRulesToScans(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) throws ReleaseRulesException {
		return applyRulesToScans(subjectScans,false);
	}

	private void nullPriorValues(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		errorList.clear();
		warningList.clear();
		for (XnatMrscandata scan : subjectScans.keySet()) {
			scan.setSubjectsessionnum(null);
			scan.setDatarelease(null);
			scan.setDbsession(null);
			scan.setDbid(null);
			scan.setDbtype(null);
			scan.setDbdesc(null);
			scan.setReleasecountscan(null);
			scan.setReleasecountscanoverride(null);
			scan.setTargetforrelease(null);
			// IMPORTANT:  Do not null out releaseOverride.  It is set by users, not by this class to force publish/non-publish.
			scan.setParameters_protocolphaseencodingdir(null);
			scan.setParameters_shimgroup(null);
			scan.setParameters_sefieldmapgroup(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_gefieldmapgroup(null);
			scan.setParameters_perotation(null);
			scan.setParameters_peswap(null);
			scan.setParameters_pedirection(null);
			scan.setParameters_readoutdirection(null);
			scan.setParameters_eprimescriptnum(null);
			scan.setParameters_scanorder(null);
			scan.setScancomplete(null);
			scan.setViewscan(null);
		}
	}

	private void setCountScanAndSession(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		ArrayList<Object> dayList = new ArrayList<Object>();
		HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (XnatMrscandata scan : subjectScans.keySet()) {
			XnatMrsessiondata currSess = subjectScans.get(scan);
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
				getErrorOrWarningList().add("RULESERROR: Scan quality rating is invalid (SCAN=" + scan.getId() +
							", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
				scan.setReleasecountscan(false);
				continue;
			}
			scan.setReleasecountscan(true);
		}
	}

	private void setTargetForReleaseAndScanID(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (XnatMrscandata scan : subjectScans.keySet()) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
				scan.setTargetforrelease(false);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					scan.getQuality().equalsIgnoreCase("poor") ||
					// 4)  COUNTSCAN - Exclude GE FieldMaps except for structural sessions
					(scan.getSeriesDescription().matches(FIELDMAP_RGX) && notStructuralSession(subjectScans.get(scan))) ||
					// 5)  COUNTSCAN - Other 7T exclusions that we want to include in the scan count
					scan.getType().matches(EJA_RGX) || scan.getSeriesDescription().matches(EJA_RGX) || 
					scan.getType().matches(DWIEXCLUDED_RGX) || scan.getSeriesDescription().matches(DWIEXCLUDED_RGX)
				) {
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		//// 7T uses the 3T structurals, so we will not do hasValidT1andT2 check for 7T
	}

	private void setTypeDescriptionAndOthers(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// These are set only for scans targeted for release
		for (XnatMrscandata scan : subjectScans.keySet()) {
			if (!scan.getTargetforrelease()) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Currently no changes for scan type
			scan.setDbtype(scan.getType());
			// SpinEchoFieldmaps
			String currSeriesDesc = scan.getSeriesDescription();
			// Initially set Dbdesc to current)  description
			scan.setDbdesc(currSeriesDesc);
			if (currSeriesDesc.equalsIgnoreCase("BOLD_PA_SE")) {
				scan.setDbdesc("SpinEchoFieldMap_PA");
				setPEFields(scan,currSeriesDesc);
			} else if (currSeriesDesc.equalsIgnoreCase("BOLD_PA_SE_SBRef")) {
				scan.setDbdesc("SpinEchoFieldMap_PA_SBRef");
			} else if (currSeriesDesc.equalsIgnoreCase("BOLD_AP_SE")) {
				scan.setDbdesc("SpinEchoFieldMap_AP");
				setPEFields(scan,currSeriesDesc);
			} else if (currSeriesDesc.equalsIgnoreCase("BOLD_AP_SE_SBRef")) {
				scan.setDbdesc("SpinEchoFieldMap_AP_SBRef");
			} else if (currSeriesDesc.contains("SpinEchoFieldMap") && !currSeriesDesc.contains("_SBRef")) {
				scan.setDbdesc(currSeriesDesc);
				setPEFields(scan,currSeriesDesc);
			} else if (currSeriesDesc.equalsIgnoreCase("FieldMap_diffusion")) {
				if (scan.getParameters_imagetype().contains("\\M\\")) {
					scan.setDbdesc(currSeriesDesc + "_Magnitude");
				} else if (scan.getParameters_imagetype().contains("\\P\\")) {
					scan.setDbdesc(currSeriesDesc + "_Phase");
				}
			}
			if (scan.getType().matches(TFMRI_RGX)) {
				// Don't set these for now for 7T.
				//if (tfmrino!=null && tfmrino.length()>1) {
				//	try {
				//		scan.setParameters_eprimescriptnum(Integer.parseInt(tfmrino.replaceFirst("_.*$","")));
				//	} catch (NumberFormatException e) {	
				//		// Do nothing for now.
				//	}
				//}
				if (currSeriesDesc.contains("_RET1_")) {
					scan.setDbdesc(scan.getSeriesDescription().replaceFirst("_RET1_", "_RETCCW_").replaceFirst(BOLDONLY_RGX,"tfMRI"));
				} else if (currSeriesDesc.contains("_RET2_")) {
					scan.setDbdesc(scan.getSeriesDescription().replaceFirst("_RET2_", "_RETCW_").replaceFirst(BOLDONLY_RGX,"tfMRI"));
				} else if (currSeriesDesc.contains("_RET3_")) {
					scan.setDbdesc(scan.getSeriesDescription().replaceFirst("_RET3_", "_RETEXP_").replaceFirst(BOLDONLY_RGX,"tfMRI"));
				} else if (currSeriesDesc.contains("_RET4_")) {
					scan.setDbdesc(scan.getSeriesDescription().replaceFirst("_RET4_", "_RETCON_").replaceFirst(BOLDONLY_RGX,"tfMRI"));
				} else if (currSeriesDesc.contains("_RET5_")) {
					scan.setDbdesc(scan.getSeriesDescription().replaceFirst("_RET5_", "_RETBAR1_").replaceFirst(BOLDONLY_RGX,"tfMRI"));
				} else if (currSeriesDesc.contains("_RET6_")) {
					scan.setDbdesc(scan.getSeriesDescription().replaceFirst("_RET6_", "_RETBAR2_").replaceFirst(BOLDONLY_RGX,"tfMRI"));
				} else {
					scan.setDbdesc(currSeriesDesc.replaceFirst(BOLDONLY_RGX, "tfMRI"));
				} 
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(RFMRI_RGX)) {
				scan.setDbdesc(currSeriesDesc.replaceFirst(BOLDONLY_RGX, "rfMRI"));
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(DMRI_RGX)) {
				setPEFields(scan,currSeriesDesc);
			}
			scan.setViewscan(!scan.getType().matches("(" + ANYFIELDMAP_RGX + ")|(" + BIAS_RGX + ")|(" + SBREF_RGX + ")"));
		}
	}

	private void updaterfMRIDescAndFlagError(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		Integer prevRestSession = null;
		List<String> sdList = Lists.newArrayList();
		int sessionRestCount = 0;
		int sessionRestAPCount = 0;
		int sessionRestPACount = 0;
		for (XnatMrscandata scan : subjectScans.keySet()) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (sdList.contains(scan.getSeriesDescription())) {
					getErrorOrWarningList().add("RULESERROR:  Session contains more than one usable resting state scan with the same description (SCAN=" + scan.getSeriesDescription()  + ")");
				} else {
					sdList.add(scan.getSeriesDescription());
				}
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						sessionRestCount=1;
						sessionRestAPCount=1;
						sessionRestPACount=1;
					} else {
						sessionRestCount++;
						if (currDbdesc.contains("_AP")) {
							sessionRestAPCount++;
						} else if (currDbdesc.contains("_PA")) {
							sessionRestPACount++;
						}
						if (sessionRestCount>4) {
							getErrorOrWarningList().add("RULESERROR:  Session contains more than four usable Resting State scans (SESSION=" + subjectScans.get(scan).getLabel() + ")");
						}
						if (sessionRestPACount>2) {
							getErrorOrWarningList().add("RULESERROR:  Session contains more than two usable Resting State scans of PA rotation (SESSION=" + subjectScans.get(scan).getLabel() + ")");
						}
						if (sessionRestAPCount>2) {
							getErrorOrWarningList().add("RULESERROR:  Session contains more than two usable Resting State scans of AP rotation (SESSION=" + subjectScans.get(scan).getLabel() + ")");
						}
					}
				}
			} 
		}
	}
	
	/*
	private boolean scanShimMatchesFieldMaps(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans, XnatMrscandata comparescan) {
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && scan.getTargetforrelease() &&
						scan.getParameters_gefieldmapgroup().equals(comparescan.getParameters_gefieldmapgroup())) {
				if (scan.getParameters_shimgroup().equals(comparescan.getParameters_shimgroup())) {
					return true;
				} else {
					return false;
				}
			} 
		}
		return false;
	}

	private void setNoRelease(XnatMrscandata scan) {
		// Do this instead of just setting targetforrelease to false to clear out other fields that
		// may have been set (Note that a few aren't included here that should remain)
		scan.setTargetforrelease(false);
		scan.setSubjectsessionnum(null);
		scan.setDatarelease(null);
		scan.setDbsession(null);
		scan.setDbid(null);
		scan.setDbtype(null);
		scan.setDbdesc(null);
		scan.setParameters_protocolphaseencodingdir(null);
		scan.setParameters_sefieldmapgroup(null);
		scan.setParameters_gefieldmapgroup(null);
		scan.setParameters_perotation(null);
		scan.setParameters_peswap(null);
		scan.setParameters_pedirection(null);
		scan.setParameters_readoutdirection(null);
		scan.setParameters_eprimescriptnum(null);
		scan.setParameters_biasgroup(null);
	}

	private char getShimGroup(XnatMrscandata scan) {
		try {
			if (scan.getParameters_shimgroup() == null || scan.getParameters_shimgroup().length()<1) {
				return '0';
			}
			return scan.getParameters_shimgroup().charAt(0);
		} catch (Exception e) {
			return '0';
		}
	}
	*/
	
	private void setGroupValues(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		
		int prevSeFieldmapGroup = 0;
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		int geFieldmapGroup = 0;
		int geSession = 0;
		boolean inGEFieldmapGroup = false;
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		
		final ArrayList<String> shimList =  Lists.newArrayList();
		final ArrayList<String> directionList =  Lists.newArrayList();
		boolean haveBothSE = false;
		final String prevSEDir = "";
		for (XnatMrscandata scan : subjectScans.keySet()) {
			
			// SHIM GROUP 
			if (scan.getTargetforrelease()) {
				List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// Per meeting on 12/5/2014, we are setting bias groups based on AAHScout scans 
			if (scan.getSeriesDescription().matches(SCOUT_BIAS_RGX)) {
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
			} else if (scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			
			
			// SE FIELDMAP GROUP - STEP 1 - SET VALUES FOR FIELDMAP SCANS
			if (scan.getType().matches(SEFIELDMAP_RGX) && scan.getTargetforrelease() && !inSEFieldmapGroup) {
				directionList.clear();
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && !scan.getType().matches(BIAS_RGX) &&
						scan.getTargetforrelease() && scan.getSubjectsessionnum().intValue() == seSession && directionList.size()>1) {
				inSEFieldmapGroup = false;
				//if (seFieldmapGroup>0) {
				//	scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				//}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup && directionList.size()>1) {
				inSEFieldmapGroup = false;
			} else if (scan.getTargetforrelease() && inSEFieldmapGroup && scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			if (scan.getType().matches(SEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				final String dir = (scan.getSeriesDescription().contains("_AP")) ? "AP" : "PA";
				if (!directionList.contains(dir)) {
					directionList.add(dir);
				}
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			
			
			// GE FIELDMAP GROUP (STEP 1 - SET VALUES FOR FIELDMAP SCANS) 
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && scan.getTargetforrelease() && !inGEFieldmapGroup) {
				inGEFieldmapGroup = true;
				geFieldmapGroup++;
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_gefieldmapgroup(geFieldmapGroup);
			} else if (!scan.getSeriesDescription().matches(GEFIELDMAP_RGX)) {
				inGEFieldmapGroup = false;
			} else if (scan.getTargetforrelease() && inGEFieldmapGroup && scan.getSubjectsessionnum().intValue() == geSession) {
				scan.setParameters_gefieldmapgroup(geFieldmapGroup);
			}
		}
		
		// GE FieldMap group (STEP 2 - ASSIGN GROUP VALUE TO T1/T2 SCANS (uses reverse sort data)
		geFieldmapGroup = 0;
		String shimGroup = null;
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				geFieldmapGroup = scan.getParameters_gefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			} 
		}
		
		// GE FieldMap group (STEP 3 - ASSIGN GROUP VALUE TO LATER T1/T2 SCANS if shim values match (DB-1841))
		geFieldmapGroup = 0;
		shimGroup = null;
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).keySet()) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				geFieldmapGroup = scan.getParameters_gefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			}	
		}
		
		// GE FieldMap group (STEP 4 - ASSIGN GROUP VALUE TO T1/T2 SCANS (uses reverse sort data) - ORIGINAL ASSIGNMENT CODE - DOESN'T LOOK AT SHIM GROUPS
		// This step can probably be removed, as it hopefully won't match and set anything, but it is being left in so session building behavior 
		// isn't unexpectedly changed.  Prior iterations will select optimal fieldmap based on shim group.  This will select when there is no
		// matching shim group, which would have happened originally.
		geFieldmapGroup = 0;
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) {
			if (scan.getSeriesDescription().matches(GEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				geSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				geFieldmapGroup = scan.getParameters_gefieldmapgroup();
			}
		}
		
		// SE FieldMap group (STEP 2 - ASSIGN GROUP VALUE TO NON-FIELDMAP SCANS (uses reverse sort data first, then forward sorted, then reverse again (setting even if shim mismatch))
		// NOTE HERE:  Per meeting 04/28 meeting, for 7T, we normally use fieldmap following the tfmri, dmri or rfmri scans, but the
		// last scans will use the one just prior (they don't have one done after).  Also, we check shim values to make sure scan
		// has same shim values as its fieldmap scan.
		seFieldmapGroup = 0;
		seSession = 0;
		shimGroup = "";
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) {
			if (scan.getType().matches(SEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				seFieldmapGroup = scan.getParameters_sefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			} else if (seSession > 0 && scan.getTargetforrelease() && !scan.getType().matches(BIAS_RGX) && scan.getSubjectsessionnum().intValue() == seSession) {
				if (seFieldmapGroup>0) {
					if (!shimGroup.equals(scan.getParameters_shimgroup())) {
						// This can happen if the subject gets out of the scanner.  Let's try the ascending match below.
						scan.setParameters_sefieldmapgroup(null);
					} else {
						scan.setParameters_sefieldmapgroup(seFieldmapGroup);
					} 
				}
			}
		} 
		seFieldmapGroup = 0;
		seSession = 0;
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).keySet()) {
			if (scan.getType().matches(SEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				seFieldmapGroup = scan.getParameters_sefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			} else if (seSession > 0 && scan.getTargetforrelease() && !scan.getType().matches(BIAS_RGX) && scan.getSubjectsessionnum().intValue() == seSession
					&& (scan.getParameters_sefieldmapgroup()==null || scan.getParameters_sefieldmapgroup()<1)) {
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
					if (!shimGroup.equals(scan.getParameters_shimgroup())) {
						getErrorOrWarningList().add("RULESERROR: Shim values do not match expected field map scan (SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		} 
		seFieldmapGroup = 0;
		seSession = 0;
		for (XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) {
			if (scan.getType().matches(SEFIELDMAP_RGX) && scan.getTargetforrelease()) {
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				seFieldmapGroup = scan.getParameters_sefieldmapgroup();
				shimGroup = scan.getParameters_shimgroup();
			} else if (seSession > 0 && scan.getTargetforrelease() && !scan.getType().matches(BIAS_RGX) && scan.getSubjectsessionnum().intValue() == seSession
					&& (scan.getParameters_sefieldmapgroup()==null || scan.getParameters_sefieldmapgroup()<1)) {
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
					if (!shimGroup.equals(scan.getParameters_shimgroup())) {
						getErrorOrWarningList().add("RULESERROR: Shim values do not match expected field map scan (SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ")");
					}
				}
			}
		} 
		
	}		

	private void rearrangeDWIDesc(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (XnatMrscandata scan : subjectScans.keySet()) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || !currDbdesc.matches(DWI_RGX)) {
				continue;
			}
			if (currDbdesc.indexOf("_AP_")>0) {
				currDbdesc = currDbdesc.replaceFirst("_AP_","_");
				currDbdesc = currDbdesc.matches(SBREF_RGX) ? currDbdesc.replaceFirst("(?i)_SBRef","_AP_SBRef") : currDbdesc + "_AP";
			}
			if (currDbdesc.indexOf("_PA_")>0) {
				currDbdesc = currDbdesc.replaceFirst("_PA_","_");
				currDbdesc = currDbdesc.matches(SBREF_RGX) ? currDbdesc.replaceFirst("(?i)_SBRef","_PA_SBRef") : currDbdesc + "_PA";
			}
			scan.setDbdesc(currDbdesc);
		}
	}
	
	private void excludeUnneededBiasScans(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (XnatMrscandata scan : subjectScans.keySet()) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if ((currDbdesc.matches(BIAS_RGX) || currDbdesc.equalsIgnoreCase("AFI")) && scan.getTargetforrelease()!=null && scan.getTargetforrelease()) {
				Integer currBias = scan.getParameters_biasgroup();
				for (XnatMrscandata scan2 : subjectScans.keySet()) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && scan2.getTargetforrelease()!=null && scan2.getTargetforrelease()) { 
						Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							break;
						}
					}
				}
				// For now, let's not exclude bias scans for 7T.  There seem only to be bias_transmit scans and only in some casesn.
				//if (!hasMatch) {
				//	scan.setTargetforrelease(false);
				//}	
				
			}
			if (currDbdesc.matches(GEFIELDMAP_RGX) && scan.getTargetforrelease()!=null && scan.getTargetforrelease()) {
				Integer currGE = scan.getParameters_gefieldmapgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans.keySet()) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && scan2.getTargetforrelease()!=null && scan2.getTargetforrelease()) { 
						Integer compareGE = scan2.getParameters_gefieldmapgroup();
						if (currGE!=null && compareGE!=null && compareGE.equals(currGE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(SPINECHO_RGX) && scan.getTargetforrelease()!=null && scan.getTargetforrelease()) {
				Integer currSE = scan.getParameters_sefieldmapgroup();
				boolean hasMatch = false;
				for (XnatMrscandata scan2 : subjectScans.keySet()) {
					String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && compareDbdesc.matches(MAINSCANDESC_RGX) && scan2.getTargetforrelease()!=null && scan2.getTargetforrelease()) { 
						Integer compareSE = scan2.getParameters_sefieldmapgroup();
						if (currSE!=null && compareSE!=null && compareSE.equals(currSE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}

	private void setScanOrderAndScanComplete(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
        HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (XnatMrscandata scan : subjectScans.keySet()) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || scan.getTargetforrelease() == null || !scan.getTargetforrelease() || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DWI_RGX)) ? "DWI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                String scanComplete = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	scanComplete = (frames>=900) ? "Y" : "N";
               		scanPct = (double)frames/900;
                } else if (currDbdesc.matches(DWI_RGX)) { 
                	String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		int dirNum = Integer.parseInt(dirVal);
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                } else if (currDbdesc.matches(TFMRI_RGX)) { 
                	if (currDbdesc.contains("_MOVIE1")) {
                		scanComplete = (frames>=921) ? "Y" : "N";
                		scanPct = (double)frames/921;
                	} else if (currDbdesc.contains("_MOVIE2")) {
                		scanComplete = (frames>=918) ? "Y" : "N";
                		scanPct = (double)frames/918;
                	} else if (currDbdesc.contains("_MOVIE3")) {
                		scanComplete = (frames>=915) ? "Y" : "N";
                		scanPct = (double)frames/915;
                	} else if (currDbdesc.contains("_MOVIE4")) {
                		scanComplete = (frames>=901) ? "Y" : "N";
                		scanPct = (double)frames/901;
                	} else if (currDbdesc.contains("_RET")) {
                		scanComplete = (frames>=300) ? "Y" : "N";
                		scanPct = (double)frames/300;
                	}
                }
                // Per Greg, tfMRI should have at least 75% of frames to have been considered usable.
                if (scanPct<.75) {
                	getErrorOrWarningList().add("RULESERROR: tfMRI, rfMRI and dMRI scans should have at least 75% of frames to have been marked usable (SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        for (XnatMrscandata scan : subjectScans.keySet()) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || scan.getTargetforrelease() == null || !scan.getTargetforrelease() || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	Double currScanPct = pctMap.get(currDbdesc);
	            String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            }
            }
        }
	}
	
	/*
	private void setRDFields(XnatMrscandata scan) {
		try {
			Float ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			if (scan.getParameters_orientation().equals("Sag") && scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
					ipe_rotation>=-0.53 && ipe_rotation<=0.53) {
				scan.setParameters_readoutdirection("+z");
				return;
			}
		} catch (Exception e) {
			// Do nothing
		}
		getErrorOrWarningList().add("RULESERROR: Values do not match expected for setting READOUT DIRECTION (SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
	}
	*/
	
	private void setPEFields(XnatMrscandata scan,String seriesDesc) {
		try {
			final Float ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			final Integer ipe_dirpos = Integer.parseInt(scan.getParameters_inplanephaseencoding_directionpositive());
			if (scan.getParameters_orientation().equals("Tra")) {
				if (scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
						ipe_rotation>=1.05 && ipe_rotation<=2.10) {
					if (ipe_dirpos==0)	{
						scan.setParameters_pedirection("-x");
						checkPESeriesDesc(scan);
						return;
					} else if (ipe_dirpos==1) {
						scan.setParameters_pedirection("+x");
						checkPESeriesDesc(scan);
						return;
					}
				} else if (scan.getParameters_inplanephaseencoding_direction().equals("COL") &&
						ipe_rotation>=-0.52 && ipe_rotation<=0.52) {
					if (ipe_dirpos==0)	{
						scan.setParameters_pedirection("+y");
						checkPESeriesDesc(scan);
						return;
					} else if (ipe_dirpos==1) {
						scan.setParameters_pedirection("-y");
						checkPESeriesDesc(scan);
						return;
					}
				}
			}
		} catch (Exception e) {
			// Do nothing
		}
		getErrorOrWarningList().add("RULESERROR: Values do not match expected for setting PHASE ENCODING DIRECTION (SCAN=" + scan.getId() + ", DESC=" + seriesDesc + ")");
	}

	private void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getParameters_pedirection().equals("+x") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("-x") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("+y") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("-y") && scan.getSeriesDescription().contains("_AP"))) {
			getErrorOrWarningList().add("RULESERROR: Series Description does not reflect PHASE ENCODING DIRECTION (SCAN=" + scan.getId() +
					", DESC=" + scan.getSeriesDescription() + ",PE=" + scan.getParameters_pedirection() + ")");
		}
	}

	public boolean isStructuralScan(XnatImagescandataI scan) {
		// Currently, 7T sessions have no structural scans
		return false;
	}

	private boolean notStructuralSession(XnatImagesessiondata session) {
		// Currently, 7T sessions have no structural scans
		return true;
	}
	
	private void hardCodedValues(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (XnatMrscandata scan : subjectScans.keySet()) {
			XnatMrsessiondata currSess = subjectScans.get(scan);
			if (currSess.getLabel() == null || scan.getSeriesDescription() == null) {
				continue;
			}
			// Don't set these for now for 7T.
			//if (currSess.getLabel().equalsIgnoreCase("119833_fncb") && scan.getSeriesDescription().contains("RELATIONAL1") &&
			//		scan.getParameters_eprimescriptnum()!=null && scan.getParameters_eprimescriptnum()==1) {
			//	// This one should have been RELATIONAL3
			//	scan.setParameters_eprimescriptnum(3);
			//}
		}
	}
	
	private void flagDwiError(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Flag error if multiple shim groups in TO BE RELEASED DWI scans 
		String prevShim = null; 
		for (final XnatMrscandata scan : subjectScans.keySet()) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(DWI_RGX)) { 
            	final String thisShim = scan.getParameters_shimgroup();
            	if (prevShim!=null && thisShim!=null && !thisShim.equals(prevShim)) {
            		getErrorOrWarningList().add("RULESERROR: DWI contains multiple bias groups in scans to be released");
            	} else if (prevShim==null && thisShim!=null) {
            		prevShim = thisShim;
            	}
            }
		}
	}

	private void flagTaskError(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
        HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        HashMap<String,Integer> sessionMap = new HashMap<String,Integer>();
        HashMap<String,String> dirMap = new HashMap<String,String>();
		for (XnatMrscandata scan : subjectScans.keySet()) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || scan.getTargetforrelease() == null || !scan.getTargetforrelease() || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(TFMRI_RGX)) { 
               	String part1=currDbdesc.replaceFirst(APPA_RGX,"");
               	// Too many scans to be released
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                	if (mapval>2) {
                		getErrorOrWarningList().add("RULESERROR: Session contains more than two task scans targeted for release (" + part1 + ")");
                	}
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
               	// Multiple sessions containing scans
                if (sessionMap.containsKey(part1) && scan.getSubjectsessionnum()!=null) {
                	if (!sessionMap.get(part1).equals(scan.getSubjectsessionnum())) {
                		getErrorOrWarningList().add("RULESERROR: Multiple sessions contain scans for the same task (" + part1 + ")");
                	}
                } else {
                	if (scan.getSubjectsessionnum()!=null) {
                		sessionMap.put(part1, scan.getSubjectsessionnum());
                	}
                }
               	// Multiple sessions containing same direction
                String direction = (currDbdesc.matches(AP_RGX)) ? "AP" : "PA";
                if (dirMap.containsKey(part1) && direction!=null) {
                	if (dirMap.get(part1).equals(direction)) {
                		getErrorOrWarningList().add("RULESERROR: Multiple sessions for same phase encoding direction for task (" + part1 + ")");
                	}
                } else {
                	if (direction!=null) {
                		dirMap.put(part1,direction);
                	}
                }
            }
		}
	}

	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta, SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
				HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt, boolean errorOverride) throws ReleaseRulesException {
		
		// Initialize values
		subjMeta.setCompleteness_imaging_fullprotocol(false);
		subjMeta.setCompleteness_imaging_t1count(0);
		subjMeta.setCompleteness_imaging_t2count(0);
		subjMeta.setCompleteness_imaging_rfmriAllscans(false);
		subjMeta.setCompleteness_imaging_rfmriComplete(false);
		subjMeta.setCompleteness_imaging_rfmriCount(0);
		subjMeta.setCompleteness_imaging_tfmriFullAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriFullComplete(false);
		subjMeta.setCompleteness_imaging_tfmriWmAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriWmComplete(false);
		subjMeta.setCompleteness_imaging_tfmriGambAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriGambComplete(false);
		subjMeta.setCompleteness_imaging_tfmriMotorAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriMotorComplete(false);
		subjMeta.setCompleteness_imaging_tfmriLangAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriLangComplete(false);
		subjMeta.setCompleteness_imaging_tfmriSocAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriSocComplete(false);
		subjMeta.setCompleteness_imaging_tfmriRelAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriRelComplete(false);
		subjMeta.setCompleteness_imaging_tfmriEmotAllscans(false);
		subjMeta.setCompleteness_imaging_tfmriEmotComplete(false);
		subjMeta.setCompleteness_imaging_dmriAllscans(false);
		subjMeta.setCompleteness_imaging_dmriCount(0);
		subjMeta.setCompleteness_imaging_dmriPctcomplete(0.0);
		subjMeta.setCompleteness_imaging_dmriComplete(false);
		
		subjMeta.setCompleteness_nontoolbox_fullbattery(false);
		subjMeta.setCompleteness_nontoolbox_hcppnp(false);
		subjMeta.setCompleteness_nontoolbox_ddisc(false);
		subjMeta.setCompleteness_nontoolbox_neo(false);
		subjMeta.setCompleteness_nontoolbox_spcptnl(false);
		subjMeta.setCompleteness_nontoolbox_cpw(false);
		subjMeta.setCompleteness_nontoolbox_pmat24a(false);
		subjMeta.setCompleteness_nontoolbox_vsplot24(false);
		subjMeta.setCompleteness_nontoolbox_er40(false);
		subjMeta.setCompleteness_nontoolbox_asrsyndrome(false);
		subjMeta.setCompleteness_nontoolbox_asrdsm(false);
		
		subjMeta.setCompleteness_toolbox_fullbattery(false);
		subjMeta.setCompleteness_toolbox_cognition(false);
		subjMeta.setCompleteness_toolbox_emotion(false);
		subjMeta.setCompleteness_toolbox_motor(false);
		subjMeta.setCompleteness_toolbox_sensory(false);
		
		subjMeta.setCompleteness_alertness_fullbattery(false);
		subjMeta.setCompleteness_alertness_mmse(false);
		subjMeta.setCompleteness_alertness_psqi(false);
		
		// In case session not up-to-date, apply release rules to scans.  The meta-data rules rely on
		// fields set during processing for the combined session, as applied in the applyRulesToScans method.
		applyRulesToScans(scanMap,errorOverride);
		
		setImagingCompleteness(subjMeta,scanMap);
		setToolboxCompleteness(subjMeta,tboxexpt);
		setNontoolboxCompleteness(subjMeta,ntexpt);
		setAlertnessCompleteness(subjMeta,visitexpt);
		
	}

	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta, SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap, HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt) throws ReleaseRulesException {
		applyRulesToSubjectMetaData(subjMeta,scanMap,tboxexpt,ntexpt,visitexpt,false);
	}
	
	private void setImagingCompleteness(HcpSubjectmetadata subjMeta,SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		
		if (subjectScans == null) {
			return;
		}
		
		int t1count=0, t2count=0;
		int rfmri_ap_count=0, rfmri_pa_count=0;
		int dmri_count=0;
		double dir71_paircomp=0, dir72_paircomp=0;
		boolean rfmri_complete = true; 
		
		// Here, we're assuming standardized series descriptions (which should be the case) and we're depending on the "scanComplete" 
		// field we're setting when building the combined session.
		ArrayList<String> allscansDesc = new ArrayList<String>();
		ArrayList<String> completeDesc = new ArrayList<String>();
		for (XnatMrscandata scan : subjectScans.keySet()) {
			if (scan.getTargetforrelease() && scan.getDbdesc()!=null) {
				String currDbdesc = scan.getDbdesc();
               	allscansDesc.add(currDbdesc);
                if ((currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(TFMRI_RGX)) && !currDbdesc.matches(SBREF_RGX)) {
                	// Only add to completeDesc if scanComplete for TFMRI, RFMRI 
                	if (scan.getScancomplete()!=null && scan.getScancomplete().equalsIgnoreCase("Y") &&
                			scan.getParameters_biasgroup()!=null && scan.getParameters_biasgroup()>0 &&
                			scan.getParameters_sefieldmapgroup()!=null && scan.getParameters_sefieldmapgroup()>0) {
                		completeDesc.add(currDbdesc);
                	}
                } else if ((currDbdesc.matches(DWI_RGX) && !currDbdesc.matches(SBREF_RGX))) {
                	if (scan.getScancomplete()!=null && scan.getScancomplete().equalsIgnoreCase("Y") &&
                			scan.getParameters_biasgroup()!=null && scan.getParameters_biasgroup()>0) {
                		completeDesc.add(currDbdesc);
                	}
                } else {
                	completeDesc.add(currDbdesc);
                } 
			}
		}
		for (XnatMrscandata scan : subjectScans.keySet()) {
			if (scan.getTargetforrelease()) {
				// T1, T2 counts
				if (scan.getType().matches(T1_RGX)) {
					t1count++;
					continue;
				}
				if (scan.getType().matches(T2_RGX)) {
					t2count++;
					continue;
				}
				String currDbdesc = scan.getDbdesc();
                String direction = (currDbdesc!=null && currDbdesc.matches(AP_RGX)) ? "AP" : "PA";
				// rfMRI Counts
                if (currDbdesc.matches(RFMRI_RGX) && !currDbdesc.matches(SBREF_RGX)) {
                	if (direction.equals("AP")) {
                		rfmri_ap_count++;
                	} else if (direction.equals("PA")) {
                		rfmri_pa_count++;
                	}
                	rfmri_complete = (rfmri_complete && completeDesc.contains(currDbdesc) && completeDesc.contains(currDbdesc + "_SBRef")) ? true : false;
      			}
			}
		}
		subjMeta.setCompleteness_imaging_t1count(t1count);
		subjMeta.setCompleteness_imaging_t2count(t2count);
		subjMeta.setCompleteness_imaging_rfmriAllscans((rfmri_ap_count>=2 && rfmri_pa_count>=2));
		subjMeta.setCompleteness_imaging_rfmriComplete((rfmri_complete && rfmri_ap_count>=2 && rfmri_pa_count>=2));
		
		subjMeta.setCompleteness_imaging_rfmriCount(rfmri_ap_count + rfmri_pa_count);
		subjMeta.setCompleteness_imaging_tfmriWmAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_WM_PA_SBRef","tfMRI_WM_PA","tfMRI_WM_AP_SBRef","tfMRI_WM_AP"})));
		subjMeta.setCompleteness_imaging_tfmriWmComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_WM_PA_SBRef","tfMRI_WM_PA","tfMRI_WM_AP_SBRef","tfMRI_WM_AP"})));
		subjMeta.setCompleteness_imaging_tfmriGambAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_GAMBLING_PA_SBRef","tfMRI_GAMBLING_PA","tfMRI_GAMBLING_AP_SBRef","tfMRI_GAMBLING_AP"})));
		subjMeta.setCompleteness_imaging_tfmriGambComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_GAMBLING_PA_SBRef","tfMRI_GAMBLING_PA","tfMRI_GAMBLING_AP_SBRef","tfMRI_GAMBLING_AP"})));
		subjMeta.setCompleteness_imaging_tfmriMotorAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_MOTOR_PA_SBRef","tfMRI_MOTOR_PA","tfMRI_MOTOR_AP_SBRef","tfMRI_MOTOR_AP"})));
		subjMeta.setCompleteness_imaging_tfmriMotorComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_MOTOR_PA_SBRef","tfMRI_MOTOR_PA","tfMRI_MOTOR_AP_SBRef","tfMRI_MOTOR_AP"})));
		subjMeta.setCompleteness_imaging_tfmriLangAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_LANGUAGE_PA_SBRef","tfMRI_LANGUAGE_PA","tfMRI_LANGUAGE_AP_SBRef","tfMRI_LANGUAGE_AP"})));
		subjMeta.setCompleteness_imaging_tfmriLangComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_LANGUAGE_PA_SBRef","tfMRI_LANGUAGE_PA","tfMRI_LANGUAGE_AP_SBRef","tfMRI_LANGUAGE_AP"})));
		subjMeta.setCompleteness_imaging_tfmriSocAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_SOCIAL_PA_SBRef","tfMRI_SOCIAL_PA","tfMRI_SOCIAL_AP_SBRef","tfMRI_SOCIAL_AP"})));
		subjMeta.setCompleteness_imaging_tfmriSocComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_SOCIAL_PA_SBRef","tfMRI_SOCIAL_PA","tfMRI_SOCIAL_AP_SBRef","tfMRI_SOCIAL_AP"})));
		subjMeta.setCompleteness_imaging_tfmriRelAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_RELATIONAL_PA_SBRef","tfMRI_RELATIONAL_PA","tfMRI_RELATIONAL_AP_SBRef","tfMRI_RELATIONAL_AP"})));
		subjMeta.setCompleteness_imaging_tfmriRelComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_RELATIONAL_PA_SBRef","tfMRI_RELATIONAL_PA","tfMRI_RELATIONAL_AP_SBRef","tfMRI_RELATIONAL_AP"})));
		subjMeta.setCompleteness_imaging_tfmriEmotAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"tfMRI_EMOTION_PA_SBRef","tfMRI_EMOTION_PA","tfMRI_EMOTION_AP_SBRef","tfMRI_EMOTION_AP"})));
		subjMeta.setCompleteness_imaging_tfmriEmotComplete(completeDesc.containsAll(Arrays.asList(new String[]{"tfMRI_EMOTION_PA_SBRef","tfMRI_EMOTION_PA","tfMRI_EMOTION_AP_SBRef","tfMRI_EMOTION_AP"})));
		subjMeta.setCompleteness_imaging_tfmriFullAllscans(subjMeta.getCompleteness_imaging_tfmriWmAllscans() && subjMeta.getCompleteness_imaging_tfmriGambAllscans() && subjMeta.getCompleteness_imaging_tfmriMotorAllscans() &&
													subjMeta.getCompleteness_imaging_tfmriLangAllscans() && subjMeta.getCompleteness_imaging_tfmriSocAllscans() &&
													subjMeta.getCompleteness_imaging_tfmriRelAllscans() && subjMeta.getCompleteness_imaging_tfmriEmotAllscans());
		subjMeta.setCompleteness_imaging_tfmriFullComplete(subjMeta.getCompleteness_imaging_tfmriWmComplete() && subjMeta.getCompleteness_imaging_tfmriGambComplete() && subjMeta.getCompleteness_imaging_tfmriMotorComplete() &&
													subjMeta.getCompleteness_imaging_tfmriLangComplete() && subjMeta.getCompleteness_imaging_tfmriSocComplete() &&
													subjMeta.getCompleteness_imaging_tfmriRelComplete() && subjMeta.getCompleteness_imaging_tfmriEmotComplete());
		subjMeta.setCompleteness_imaging_dmriAllscans(allscansDesc.containsAll(Arrays.asList(new String[]{"DWI_dir71_PA_SBRef","DWI_dir71_PA","DWI_dir71_AP_SBRef","DWI_dir71_AP",
																									"DWI_dir72_PA_SBRef","DWI_dir72_PA","DWI_dir72_AP_SBRef","DWI_dir72_AP"})));
		subjMeta.setCompleteness_imaging_dmriComplete(completeDesc.containsAll(Arrays.asList(new String[]{"DWI_dir71_PA_SBRef","DWI_dir71_PA","DWI_dir71_AP_SBRef","DWI_dir71_AP",
																									"DWI_dir72_PA_SBRef","DWI_dir72_PA","DWI_dir72_AP_SBRef","DWI_dir72_AP"})));
		subjMeta.setCompleteness_imaging_fullprotocol(subjMeta.getCompleteness_imaging_t1count()>0 && subjMeta.getCompleteness_imaging_t2count()>0 &&
														subjMeta.getCompleteness_imaging_rfmriComplete() && subjMeta.getCompleteness_imaging_tfmriFullComplete() &&
														subjMeta.getCompleteness_imaging_dmriComplete());
		
		subjMeta.setCompleteness_imaging_dmriCount(dmri_count);
		subjMeta.setCompleteness_imaging_dmriPctcomplete(((dir71_paircomp*71)+(dir72_paircomp*72))/(71+72));
	}

	private void setNontoolboxCompleteness(HcpSubjectmetadata subjMeta, NtScores ntexpt) {
		
		if (ntexpt == null) {
			return;
		}
		
		if (ntexpt.getHcppnp_marsFinal()!=null && ntexpt.getHcppnp_marsErrs()!=null && ntexpt.getHcppnp_marsLogScore()!=null) {
			subjMeta.setCompleteness_nontoolbox_hcppnp(true);
		}
		if (ntexpt.getDdisc_auc200()!=null && ntexpt.getDdisc_auc200()>=0 && ntexpt.getDdisc_auc40000()!=null && ntexpt.getDdisc_auc40000()>=0) {
			subjMeta.setCompleteness_nontoolbox_ddisc(true);
		}
		if (ntexpt.getNeo_neo()!=null && ntexpt.getNeo_neo()>0) {
			subjMeta.setCompleteness_nontoolbox_neo(true);
		}
		if (ntexpt.getSpcptnl_scptSen()!=null && ntexpt.getSpcptnl_scptSen()>=0 &&
		    ntexpt.getSpcptnl_scptSpec()!=null && ntexpt.getSpcptnl_scptSpec()>=0 &&
		    ntexpt.getSpcptnl_scptTprt()!=null && ntexpt.getSpcptnl_scptTprt()>=0 &&
		    ntexpt.getSpcptnl_scptLrnr()!=null && ntexpt.getSpcptnl_scptLrnr()>=0) {
			subjMeta.setCompleteness_nontoolbox_spcptnl(true);
		}
		if (ntexpt.getCpw_iwrdRtc()!=null && ntexpt.getCpw_iwrdRtc()>=0 &&
				ntexpt.getCpw_iwrdTot()!=null && ntexpt.getCpw_iwrdTot()>=0) {
			subjMeta.setCompleteness_nontoolbox_cpw(true);
		}
		if (ntexpt.getPmat24a_pmat24Acr()!=null && ntexpt.getPmat24a_pmat24Acr()>=0 &&
				ntexpt.getPmat24a_pmat24Asi()!=null && ntexpt.getPmat24a_pmat24Asi()>=0 &&
				ntexpt.getPmat24a_pmat24Artcr()!=null && ntexpt.getPmat24a_pmat24Artcr()>=0) {
			subjMeta.setCompleteness_nontoolbox_pmat24a(true);
		}
		if (ntexpt.getVsplot24_vsplotCrte()!=null && ntexpt.getVsplot24_vsplotCrte()>=0 &&
		    ntexpt.getVsplot24_vsplotOff()!=null && ntexpt.getVsplot24_vsplotOff()>=0 &&
		    ntexpt.getVsplot24_vsplotTc()!=null && ntexpt.getVsplot24_vsplotTc()>=0) {
			subjMeta.setCompleteness_nontoolbox_vsplot24(true);
		}
		if (ntexpt.getEr40_er40Cr()!=null && ntexpt.getEr40_er40Cr()>=0 &&
				ntexpt.getEr40_er40Crt()!=null && ntexpt.getEr40_er40Crt()>=0 &&
				ntexpt.getEr40_er40ang()!=null && ntexpt.getEr40_er40ang()>=0 &&
				ntexpt.getEr40_er40fear()!=null && ntexpt.getEr40_er40fear()>=0 &&
				ntexpt.getEr40_er40hap()!=null && ntexpt.getEr40_er40hap()>=0 &&
				ntexpt.getEr40_er40noe()!=null && ntexpt.getEr40_er40noe()>=0 &&
				ntexpt.getEr40_er40sad()!=null && ntexpt.getEr40_er40sad()>=0) {
			subjMeta.setCompleteness_nontoolbox_er40(true);
		}
		if (ntexpt.getAsr_asrsyndromescores_asrCmpTotalRaw()!=null && ntexpt.getAsr_asrsyndromescores_asrCmpTotalRaw()>0 &&
				ntexpt.getAsr_asrsyndromescores_asrCmpTotalT()!=null && ntexpt.getAsr_asrsyndromescores_asrCmpTotalT()>0) {
			subjMeta.setCompleteness_nontoolbox_asrsyndrome(true);
		}
		if (ntexpt.getAsr_asrdsmscores_dsmDepRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmDepRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAnxRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAnxRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmSomRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmSomRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAvoidRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAvoidRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAdhRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAdhRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmInattRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmInattRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmHypRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmHypRaw()>=0 &&
				ntexpt.getAsr_asrdsmscores_dsmAsocRaw()!=null && ntexpt.getAsr_asrdsmscores_dsmAsocRaw()>=0) {
			subjMeta.setCompleteness_nontoolbox_asrdsm(true);
		}
		subjMeta.setCompleteness_nontoolbox_fullbattery(subjMeta.getCompleteness_nontoolbox_hcppnp() &&
														subjMeta.getCompleteness_nontoolbox_ddisc() &&
														subjMeta.getCompleteness_nontoolbox_neo() &&
														subjMeta.getCompleteness_nontoolbox_spcptnl() &&
														subjMeta.getCompleteness_nontoolbox_cpw() &&
														subjMeta.getCompleteness_nontoolbox_pmat24a() &&
														subjMeta.getCompleteness_nontoolbox_vsplot24() &&
														subjMeta.getCompleteness_nontoolbox_er40() &&
														subjMeta.getCompleteness_nontoolbox_asrsyndrome() &&
														subjMeta.getCompleteness_nontoolbox_asrdsm());
	}

	private void setToolboxCompleteness(HcpSubjectmetadata subjMeta,
			HcpToolboxdata tboxexpt) {
		
		if (tboxexpt == null) {
			return;
		}
		
		if (tboxexpt.getCogDimCardUscr()!=null && tboxexpt.getCogDimCardUscr()>=0 &&
		    tboxexpt.getCogDimCardAscr()!=null && tboxexpt.getCogDimCardAscr()>=0 &&
		    tboxexpt.getCogFlankerUscr()!=null && tboxexpt.getCogFlankerUscr()>=0 &&
		    tboxexpt.getCogFlankerAscr()!=null && tboxexpt.getCogFlankerAscr()>=0 &&
		    tboxexpt.getCogListSortUscr()!=null && tboxexpt.getCogListSortUscr()>=0 &&
		    tboxexpt.getCogListSortAscr()!=null && tboxexpt.getCogListSortAscr()>=0 &&
		    tboxexpt.getCogOralReadUscr()!=null && tboxexpt.getCogOralReadUscr()>=0 &&
		    tboxexpt.getCogOralReadAscr()!=null && tboxexpt.getCogOralReadAscr()>=0 &&
		    tboxexpt.getCogPatternUscr()!=null && tboxexpt.getCogPatternUscr()>=0 &&
		    tboxexpt.getCogPatternAscr()!=null && tboxexpt.getCogPatternAscr()>=0 &&
		    tboxexpt.getCogPicSeqUscr()!=null && tboxexpt.getCogPicSeqUscr()>=0 &&
		    tboxexpt.getCogPicSeqAscr()!=null && tboxexpt.getCogPicSeqAscr()>=0 &&
		    tboxexpt.getCogPicVocabUscr()!=null && tboxexpt.getCogPicVocabUscr()>=0 &&
		    tboxexpt.getCogPicVocabAscr()!=null && tboxexpt.getCogPicVocabAscr()>=0) {
		    subjMeta.setCompleteness_toolbox_cognition(true);
		}
		
		if (tboxexpt.getEmoFearSomTscr()!=null && tboxexpt.getEmoFearSomTscr()>=0 && 
		    tboxexpt.getEmoFriendTscr()!=null && tboxexpt.getEmoFriendTscr()>=0 && 
		    tboxexpt.getEmoLifeSatTscr()!=null && tboxexpt.getEmoLifeSatTscr()>=0 && 
		    tboxexpt.getEmoInstSupTscr()!=null && tboxexpt.getEmoInstSupTscr()>=0 && 
		    tboxexpt.getEmoLonelyTscr()!=null && tboxexpt.getEmoLonelyTscr()>=0 && 
		    tboxexpt.getEmoMeaningTscr()!=null && tboxexpt.getEmoMeaningTscr()>=0 && 
		    tboxexpt.getEmoHostilTscr()!=null && tboxexpt.getEmoHostilTscr()>=0 && 
		    tboxexpt.getEmoRejectTscr()!=null && tboxexpt.getEmoRejectTscr()>=0 &&
		    tboxexpt.getEmoStressTscr()!=null && tboxexpt.getEmoStressTscr()>=0 && 
		    tboxexpt.getEmoPosAffectTscr()!=null && tboxexpt.getEmoPosAffectTscr()>=0 && 
		    tboxexpt.getEmoSadnessTscr()!=null && tboxexpt.getEmoSadnessTscr()>=0 && 
		    tboxexpt.getEmoSelfEffTscr()!=null && tboxexpt.getEmoSelfEffTscr()>=0 && 
		    tboxexpt.getEmoAngerAffTscr()!=null && tboxexpt.getEmoAngerAffTscr()>=0 && 
		    tboxexpt.getEmoAngerHostilTscr()!=null && tboxexpt.getEmoAngerHostilTscr()>=0 && 
		    tboxexpt.getEmoAngerPhysTscr()!=null && tboxexpt.getEmoAngerPhysTscr()>=0 && 
		    tboxexpt.getEmoSupportTscr()!=null && tboxexpt.getEmoSupportTscr()>=0 && 
		    tboxexpt.getEmoFearAffTscr()!=null && tboxexpt.getEmoFearAffTscr()>=0) {
		    subjMeta.setCompleteness_toolbox_emotion(true);
		}
		
		if (tboxexpt.getMotGripUscr()!=null && tboxexpt.getMotGripUscr()>=0 &&
		    tboxexpt.getMotGripAscr()!=null && tboxexpt.getMotGripAscr()>=0 &&
		    tboxexpt.getMotWalk2minUscr()!=null && tboxexpt.getMotWalk2minUscr()>=0 &&
		    tboxexpt.getMotWalk2minAscr()!=null && tboxexpt.getMotWalk2minAscr()>=0 &&
		    tboxexpt.getMotWalk4mCscr()!=null && tboxexpt.getMotWalk4mCscr()>=0 && 
		    tboxexpt.getMotPegboardUscr()!=null && tboxexpt.getMotPegboardUscr()>=0 &&
		    tboxexpt.getMotPegboardAscr()!=null && tboxexpt.getMotPegboardAscr()>=0) {
		    subjMeta.setCompleteness_toolbox_motor(true);
		}
		
		if (tboxexpt.getSenOdorUscr()!=null && tboxexpt.getSenOdorUscr()>=0 &&
		    tboxexpt.getSenOdorAscr()!=null && tboxexpt.getSenOdorAscr()>=0 &&
		    tboxexpt.getSenPainTscr()!=null && tboxexpt.getSenPainTscr()>=0 &&
		    tboxexpt.getSenTasteUscr()!=null && tboxexpt.getSenTasteUscr()>=0 &&
		    tboxexpt.getSenTasteAscr()!=null && tboxexpt.getSenTasteAscr()>=0 && 
		    tboxexpt.getSenNoiseCscr()!=null && tboxexpt.getSenNoiseCscr()>=0) {
		    subjMeta.setCompleteness_toolbox_sensory(true);
		}
		
		subjMeta.setCompleteness_toolbox_fullbattery(subjMeta.getCompleteness_toolbox_motor() &&
													 subjMeta.getCompleteness_toolbox_emotion() &&
													 subjMeta.getCompleteness_toolbox_motor() &&
													 subjMeta.getCompleteness_toolbox_sensory());
		   
	}

	private void setAlertnessCompleteness(HcpSubjectmetadata subjMeta, HcpvisitHcpvisitdata visitexpt) {
		
		if (visitexpt == null) {
			return;
		}
		
		if (visitexpt.getHcpmmse_totalscore() !=null && visitexpt.getHcpmmse_totalscore()>=0) {
		    subjMeta.setCompleteness_alertness_mmse(true);
		}
		if (visitexpt.getHcppsqi_totalscore() !=null && visitexpt.getHcppsqi_totalscore()>=0) {
		    subjMeta.setCompleteness_alertness_psqi(true);
		}
		
		subjMeta.setCompleteness_alertness_fullbattery(subjMeta.getCompleteness_alertness_mmse() &&
													 subjMeta.getCompleteness_alertness_psqi());
		
	}

}


