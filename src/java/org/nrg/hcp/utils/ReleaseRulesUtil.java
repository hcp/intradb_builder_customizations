package org.nrg.hcp.utils;

import org.nrg.config.services.ConfigService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.restlet.data.Response;
import org.restlet.data.Status;

public class ReleaseRulesUtil {
	
	public static final String CONFIG_TOOL_NAME = "releaseRules";
	public static final String CONFIG_PATH_TO_FILE = "releaseRulesClass.txt";
	
	public static ReleaseRulesI getReleaseRulesForProject(XnatProjectdata proj, Response response) {
		
		final ReleaseRulesI releaseRules;
		final ConfigService configService = XDAT.getConfigService();
		String rrClass = configService.getConfigContents(CONFIG_TOOL_NAME,CONFIG_PATH_TO_FILE,
						(long)(Integer)proj.getItem().getProps().get("projectdata_info"));
		if (rrClass==null || rrClass.length()<1) {
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"A release rules class has not been configured/specified for this project");
			return null;
		}
		rrClass=rrClass.replaceAll("[^\\w.]","");
		try {
			Object releaseRulesO = Class.forName(rrClass).newInstance();
			if (releaseRulesO instanceof ReleaseRulesI) {
				releaseRules = (ReleaseRulesI) releaseRulesO;
				return releaseRules;
			} else {
				response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"The release rules class specified in the configuration for this project is invalid (" +
								rrClass + " is not an instance of org.nrg.hcp.utils.ReleaseRulesI)");
				
			}
		} catch (InstantiationException e1) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"Could not instantiate release rules class (" + rrClass + ")");
		} catch (IllegalAccessException e1) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"Could not instantiate release rules class (" + rrClass + ")");
		} catch (ClassNotFoundException e1) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"ClassNotFoundException - Could not find release rules class (" + rrClass + ")");
		} catch (Exception e1) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"Exception - " + e1.toString());
		}
		return null;
	
	}

}
