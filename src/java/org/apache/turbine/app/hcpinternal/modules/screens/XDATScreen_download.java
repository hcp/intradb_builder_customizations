//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * More generic download screen, currently limited to imageSessionData
 *
 */
package org.apache.turbine.app.hcpinternal.modules.screens;
/*
 * Provide default download interface for imageSessions - will throw exceptions for non-image session data types
 */

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;

/**
 * @author MRH
 *
 */
public class XDATScreen_download extends SecureReport {

    /* (non-Javadoc)
     * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    public void finalProcessing(RunData data, Context context) {
    	// Will throw exception for non-image sessions - NOT CURRENTLY SUPPORTED 
    	// Want logged and displayed exception, but don't want to modify XNAT superclass method. 
    	// Should the XDATActionRouter be modified to check  parent-type?
    	XnatImagesessiondata isess = ((XnatImagesessiondata)om);
	    context.put("archive",isess.listArchiveToHTML(TurbineUtils.GetRelativeServerPath(data)));
	    data.getSession().setAttribute("download_session", isess);
    }

    
    /**
     * Return null to use the defualt settings (which are configured in xdat:element_security).  Otherwise, true will force a pre-load of the item.
     * @return
     */
    public Boolean preLoad()
    {
        return Boolean.TRUE;
    }
}
