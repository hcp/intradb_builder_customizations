

function populateQcCharts(projectID) {
    $('#qccharts_tab').addClass("hidden");
    var doAJAXConfig = $.ajax({
        type : "GET",
        url : serverRoot + "/REST/projects/" + projectID + "/qcchartconfig",
        cache: false,
        async: true,
        context: this,
        dataType: 'text'
      });
    var controlLimitType = ($.cookie("control-limit-type")!=undefined) ? $.cookie("control-limit-type") : "Variable";
    // Now, let's always ask for 500 observations so we don't have to query the surver to change number of observations displayed
    //var displayNobs = ($.cookie("display-nobs")!=undefined) ? $.cookie("display-nobs") : "100";
    var displayNobs = 500;
    doAJAXConfig.done( function( data, textStatus, jqXHR ) {
        if (data == null || data.length<1) {
            // Do nothing for now
            $('#qccharts_tab').addClass("hidden");
            $('#qccharts').addClass("error").html("<span>This project has not been configured for QC Chart display.</span>");
        } else {
            $('#qccharts_tab').removeClass("hidden");
            $('#qccharts_header').html("<div id=\"qc_waiting\" class='qcchart_waiting'><img src=\"" + serverRoot + "/scripts/yui/build/assets/skins/xnat/wait.gif\"> Building QC chart(s)</div>");
            var baselinePeriod = $
            var doAJAXData = $.ajax({
                type : "GET",
                url : serverRoot + "/REST/projects/" + projectID + "/qcchartdata?baselinePeriod=" + controlLimitType + "&displayNobs=" + displayNobs,
                cache: false,
                async: true,
                context: this,
                dataType: 'json'
              });
            doAJAXData.done( function( data, textStatus, jqXHR ) {
                if (data == null) {
                    // Do nothing for now
                } else {
                    $('#qccharts_tab').data("chartData",data);
                    buildCharts(data,projectID,undefined);
                }
            });
            doAJAXData.fail( function( data, textStatus, error ) {
                // Do nothing for now
            });
        }
    });
    doAJAXConfig.fail( function( data, textStatus, error ) {
        // Do nothing for now
    });
}

function refreshDisplay(projectID,clearCache) {
    $("#qc_waiting").show();
    var controlLimitType = $("#control_limit_selector").val();
    $.cookie("control-limit-type",controlLimitType);
    var displayNobsSel = $("#display_nobs_selector").val();
    $.cookie("display-nobs",displayNobsSel);
    // Now, let's always ask for 500 observations so we don't have to query the surver to change number of observations displayed
    var displayNobs = 500;
    $("#search_field_selector_div").hide();
    $("#control_limit_selector_div").hide();
    $("#display_nobs_selector_div").hide();
    var displayArr = [];
    $("[id^='QC_SRCH_'][id$='_charts'").each(
        function(index,value) {
            var chartIt = value.id.replace(/^.*_ITEM_/,'').replace(/_charts$/,'');
            if ($.inArray(chartIt, displayArr)<0) {
                displayArr.push(chartIt);
            }
        }
    );
    var cacheAppend = (clearCache) ? "clearCache=true" : ""
    var doAJAXData = $.ajax({
        type : "GET",
        url : serverRoot + "/REST/projects/" + projectID + "/qcchartdata?baselinePeriod=" + controlLimitType + "&displayNobs=" + displayNobs + "&" + cacheAppend,
        cache: false,
        async: true,
        context: this,
        dataType: 'json'
      });
    doAJAXData.done( function( data, textStatus, jqXHR ) {
        if (data == null) {
            // Do nothing for now
        } else {
            $('#qccharts_tab').data("chartData",data);
            $("#search_field_selector_div").remove();
            $("#control_limit_selector_div").remove();
            $("#display_nobs_selector_div").remove();
            buildCharts(data,projectID,displayArr);
            $("#qc_waiting").hide();
            $("#search_field_selector_div").show();
            $("#display_nobs_selector_div").show();
        }
    });
    doAJAXData.fail( function( data, textStatus, error ) {
        // Do nothing for now
    });
}

function refreshDisplayNoServerCall(projectID) {
    var data = $('#qccharts_tab').data("chartData");
    $("#qc_waiting").show();
    var controlLimitType = $("#control_limit_selector").val();
    $.cookie("control-limit-type",controlLimitType);
    var displayNobsSel = $("#display_nobs_selector").val();
    $.cookie("display-nobs",displayNobsSel);
    $("#search_field_selector_div").hide();
    $("#control_limit_selector_div").hide();
    $("#display_nobs_selector_div").hide();
    var displayArr = [];
    $("[id^='QC_SRCH_'][id$='_charts'").each(
        function(index,value) {
            var chartIt = value.id.replace(/^.*_ITEM_/,'').replace(/_charts$/,'');
            if ($.inArray(chartIt, displayArr)<0) {
                displayArr.push(chartIt);
            }
        }
    );
    setTimeout(function() {
        $("#search_field_selector_div").remove();
        $("#control_limit_selector_div").remove();
        $("#display_nobs_selector_div").remove();
        buildCharts(data,projectID,displayArr);
        $("#qc_waiting").hide();
        $("#search_field_selector_div").show();
        $("#display_nobs_selector_div").show();
    },500);
}

function refreshQcChartCache(projectID) {
    refreshDisplay(projectID,true);
}

function displayWithNewSettings(projectID) {
    refreshDisplay(projectID,false);
}

function fieldSelect(chartIt) {
    $("#qc_waiting").show();
    $("#search_field_selector_div").hide();
    $("#control_limit_selector_div").hide();
    $("#display_nobs_selector_div").hide();
    var displayNobs = $("#display_nobs_selector").val();
    setTimeout(function() {
        var chartData = $('#qccharts_tab').data("chartData");
        $("#search_field_selector option[value=\"" + chartIt +  "\"]").remove();
        if (chartIt!="BUILD ALL CHARTS") {
            var displayObject = {};
            for (var search in chartData) {
                displayObject[search] = {};
                displayChart(chartData,search,chartIt,displayNobs,displayObject);
            }
            $("#search_field_selector_div").show();
            $("#control_limit_selector_div").show();
            $("#display_nobs_selector_div").show();
        } else {
            $("#search_field_selector option").each(function(index,value) {
                    if (value.value.length>0) {
                        var displayObject = {};
                        for (var search in chartData) {
                            displayObject[search] = {};
                            setTimeout(displayChart(chartData,search,value.value,displayNobs,displayObject),10);
                        }
                    }
                });
            $("#control_limit_selector_div").show();
            $("#display_nobs_selector_div").show();
        }
        $("#qc_waiting").hide();
    }, 100);
}

function isFirefox() {
   return typeof InstallTrigger !== 'undefined';
}

/* Not currently in use
function qcTabClick(projectID) {
    $('#qccharts_header').html("<div id=\"qc_waiting\" class='qcchart_waiting'><img src=\"" + serverRoot + "/scripts/yui/build/assets/skins/xnat/wait.gif\"> Building QC chart(s)</div>");
    var chartData = $('#qccharts_tab').data("chartData");
    // Browsers have issues with rendering SVGs when created when their tab is not active or when switching between tabs.
    // Redraw them when it becomes active.
    if (chartData != undefined) {
        buildCharts(chartData,projectID);
    }
}
*/

function buildCharts(chartData,projectID,initialFields) {
    var printStr = "";
    var controlLimitType = ($.cookie("control-limit-type")!=undefined) ? $.cookie("control-limit-type") : "Variable";
    var displayNobs = ($.cookie("display-nobs")!=undefined) ? $.cookie("display-nobs") : "100";
    var displayObject = {};
    for (var search in chartData) {

        displayObject[search] = {};
        if (! $(("#qccharts_" + search).replace(":","\\:")).length) {
            $('#qccharts').append("<div id='qccharts_" + search + "'></div>");
        } else {
            $("#qccharts_" + search).empty();
        }
        if (! $("#search_field_selector_div").length) {
            $('#qccharts_header').append("<div id='search_field_selector_div' style='width:100%;text-align:right'>" 
                    + "<div style='float:left'><a onclick=\"refreshQcChartCache('" + projectID + "')\">Refresh results cache</a></div>"  
                    + "<span class=\"tip_icon\" style=\"margin-right:3px;left:2px;top:3px;bottom:3px;\">" 
                       + "<span class=\"tip shadowed\" style=\"top:20px;z-index:10000;white-space:normal;left:-150px;width:300px;background-color:#ffc;\">"
                            + "RULE A:  Single point outside control limits<br>" 
                            + "RULE B:  2 out of 3 points in a row beyond 2&sigma;<br>" 
                            + "RULE C:  4 out of 5 points beyond 1&sigma;<br>" 
                            + "RULE D:  9 points in a row above/below center line<br>" 
                       + "</span></span><br>"
                    + "<span id='search_field_selector_text' style=padding-left:10px;padding-right:5px;'>Select additional fields to chart: </span>" 
                    + "<select id='search_field_selector' class=\"qcchart_select\" style='margin-top:2px' onchange='fieldSelect(this.value)'><option value=\"\"></option><option value=\"BUILD ALL CHARTS\">BUILD ALL CHARTS</option></select>"
                    + "</div>"
             );
            $('#qccharts_header').append("<div id='control_limit_selector_div' style='width:100%;text-align:right;padding-top:3px'>" 
                     + "<span id='control_limit_selector_text' style=padding-left:10px;padding-right:5px;'>Control limits baseline: </span>" 
                     + "<select class=\"qcchart_select\" id='control_limit_selector' onchange=\"displayWithNewSettings('" + projectID + "')\"'>" 
                     + "<option value=\"Variable\"" + ((controlLimitType=='Variable') ? 'selected' : '') + ">"
                     + "Variable - Rule D</option>" 
                     + "<option value=\"VariableAll\"" + ((controlLimitType=='VariableAll') ? 'selected' : '') + ">"
                     + "Variable - Rules B,C & D</option>" 
                     + "<option value=\"SeriesEnd\"" + ((controlLimitType=='SeriesEnd') ? 'selected' : '') + ">"
                     + "Series End</option>" 
                     + "<option value=\"SeriesBeginning\"" + ((controlLimitType=='SeriesBeginning') ? 'selected' : '') + ">"
                     + "SeriesBeginning</option></select>"
                     + "</div>");
            $('#qccharts_header').append("<div id='display_nobs_selector_div' style='width:100%;text-align:right;padding-top:3px'>" 
                     + "<span id='display_nobs_selector_text' style=padding-left:10px;padding-right:5px;'>Number of observations to display: </span>" 
                     + "<select class=\"qcchart_select\" id='display_nobs_selector' onchange=\"refreshDisplayNoServerCall('" + projectID + "')\"'>" 
                     +"<option value=\"100\"" + ((displayNobs=='100') ? 'selected' : '') + ">"
                     + "100</option>" 
                     +"<option value=\"200\"" + ((displayNobs=='200') ? 'selected' : '') + ">"
                     + "200</option>" 
                     +"<option value=\"300\"" + ((displayNobs=='300') ? 'selected' : '') + ">"
                     + "300</option>" 
                     +"<option value=\"400\"" + ((displayNobs=='400') ? 'selected' : '') + ">"
                     + "400</option>"
                     +"<option value=\"500\"" + ((displayNobs=='500') ? 'selected' : '') + ">"
                     + "500</option></select>"
                     + "</div>");
        }
        printStr = printStr + "Stored search:  " +  search + "<br/>";
        for (var chartIt in chartData[search]["charts"]) {
            if (initialFields!=undefined && initialFields.length>0 &&
                     $.inArray(chartIt, initialFields)>=0) {
                displayChart(chartData,search,chartIt,displayNobs,displayObject);
            } else if (initialFields!=undefined && initialFields.length>0 &&
                     $.inArray(chartIt, initialFields)<0) {
                if  (! $("#search_field_selector option[value=\"" + chartIt +  "\"]").length) {
                    $('#search_field_selector').append($('<option/>', { 
                        value: chartIt,
                        text : chartIt
                    }));
                }
            } else if (chartData[search]["initialDisplayFields"]!=undefined && chartData[search]["initialDisplayFields"].length>0 &&
                     $.inArray(chartIt, chartData[search]["initialDisplayFields"])>=0) {
                displayChart(chartData,search,chartIt,displayNobs,displayObject);
            } else if (chartData[search]["initialDisplayFields"]!=undefined && chartData[search]["initialDisplayFields"].length>0 &&
                     $.inArray(chartIt, chartData[search]["initialDisplayFields"])<0) {
                if  (! $("#search_field_selector option[value=\"" + chartIt +  "\"]").length) {
                    $('#search_field_selector').append($('<option/>', { 
                        value: chartIt,
                        text : chartIt
                    }));
                }
            } else {
                displayChart(chartData,search,chartIt,displayNobs,displayObject);
                $('#search_field_selector').remove();
                $('#search_field_selector_text').remove();
                $("#control_limit_selector").remove();
                $("#control_limit_selector_text").remove();
                $("#display_nobs_selector").remove();
                $("#display_nobs_selector_text").remove();
            } 
        }
        $("#qc_waiting").hide();
    } 
}

function displayChart(chartData,search,chartIt,displayNobs,displayObject) {

    displayObject[search][chartIt] = {};

    var infoVar = $.extend(true, {}, chartData[search]["SessionInfoFields"]);
    for (var infoField in infoVar) {
        for (var byField in infoVar[infoField]) {
            var objLength = infoVar[infoField][byField].length;
            infoVar[infoField][byField] = (objLength>displayNobs) ? infoVar[infoField][byField].splice(objLength-displayNobs,objLength) : infoVar[infoField][byField];
        }
    }

    for (byValue in chartData[search]["charts"][chartIt]) {

            displayObject[search][chartIt][byValue] = {};

            var heightvar = 100;

            var outerChartDiv = "QC_SRCH_" + search + "_BYVALUE_" + byValue +  "_ITEM_" + chartIt + "_charts";
            var innerChartDiv = "QC_SRCH_" + search + "_BYVALUE_" + byValue +  "_ITEM_" + chartIt + "_chart";
            var innerMrChartDiv = "QC_SRCH_" + search + "_BYVALUE_" + byValue + "_ITEM_" + chartIt + "_mrchart";
            try {
                var plotData_Arr = chartData[search]["charts"][chartIt][byValue]["RawData"];
                var plotData = plotData_Arr.slice(((plotData_Arr.length>displayNobs) ? plotData_Arr.length-displayNobs : 0),plotData_Arr.length);
                var plotData_CL_Arr = chartData[search]["charts"][chartIt][byValue]["CL"];
                var plotData_CL = plotData_CL_Arr.slice(((plotData_CL_Arr.length>displayNobs) ? plotData_CL_Arr.length-displayNobs : 0),plotData_CL_Arr.length);
                var plotData_UCL_Arr = chartData[search]["charts"][chartIt][byValue]["UCL"];
                var plotData_UCL = plotData_UCL_Arr.slice(((plotData_UCL_Arr.length>displayNobs) ? plotData_UCL_Arr.length-displayNobs : 0),plotData_UCL_Arr.length);
                var plotData_USD1_Arr = chartData[search]["charts"][chartIt][byValue]["USD1"];
                var plotData_USD1 = plotData_USD1_Arr.slice(((plotData_USD1_Arr.length>displayNobs) ? plotData_USD1_Arr.length-displayNobs : 0),plotData_USD1_Arr.length);
                var plotData_USD2_Arr = chartData[search]["charts"][chartIt][byValue]["USD2"];
                var plotData_USD2 = plotData_USD2_Arr.slice(((plotData_USD2_Arr.length>displayNobs) ? plotData_USD2_Arr.length-displayNobs : 0),plotData_USD2_Arr.length);
                var plotData_LCL_Arr = chartData[search]["charts"][chartIt][byValue]["LCL"];
                var plotData_LCL = plotData_LCL_Arr.slice(((plotData_LCL_Arr.length>displayNobs) ? plotData_LCL_Arr.length-displayNobs : 0),plotData_LCL_Arr.length);
                var plotData_LSD1_Arr = chartData[search]["charts"][chartIt][byValue]["LSD1"];
                var plotData_LSD1 = plotData_LSD1_Arr.slice(((plotData_LSD1_Arr.length>displayNobs) ? plotData_LSD1_Arr.length-displayNobs : 0),plotData_LSD1_Arr.length);
                var plotData_LSD2_Arr = chartData[search]["charts"][chartIt][byValue]["LSD2"];
                var plotData_LSD2 = plotData_LSD2_Arr.slice(((plotData_LSD2_Arr.length>displayNobs) ? plotData_LSD2_Arr.length-displayNobs : 0),plotData_LSD2_Arr.length);

                plotData.unshift("plotData");
                plotData_CL.unshift("plotData_CL");
                plotData_UCL.unshift("plotData_UCL");
                plotData_USD1.unshift("plotData_USD1");
                plotData_USD2.unshift("plotData_USD2");
                plotData_LCL.unshift("plotData_LCL");
                plotData_LSD1.unshift("plotData_LSD1");
                plotData_LSD2.unshift("plotData_LSD2");

                displayObject[search][chartIt][byValue]["plotData"] = plotData;
                displayObject[search][chartIt][byValue]["plotData_CL"] = plotData_CL;
                displayObject[search][chartIt][byValue]["plotData_UCL"] = plotData_UCL;
                displayObject[search][chartIt][byValue]["plotData_USD1"] = plotData_USD1;
                displayObject[search][chartIt][byValue]["plotData_USD2"] = plotData_USD2;
                displayObject[search][chartIt][byValue]["plotData_LCL"] = plotData_LCL;
                displayObject[search][chartIt][byValue]["plotData_LSD1"] = plotData_LSD1;
                displayObject[search][chartIt][byValue]["plotData_LSD2"] = plotData_LSD2;
           
                var mrPlotData_Arr = chartData[search]["charts"][chartIt][byValue]["MR"];
                var mrPlotData = mrPlotData_Arr.slice(((mrPlotData_Arr.length>displayNobs) ? mrPlotData_Arr.length-displayNobs : 0),mrPlotData_Arr.length);
                var mrPlotData_CL_Arr = chartData[search]["charts"][chartIt][byValue]["MR_CL"];
                var mrPlotData_CL = mrPlotData_CL_Arr.slice(((mrPlotData_CL_Arr.length>displayNobs) ? mrPlotData_CL_Arr.length-displayNobs : 0),mrPlotData_CL_Arr.length);
                var mrPlotData_UCL_Arr = chartData[search]["charts"][chartIt][byValue]["MR_UCL"];
                var mrPlotData_UCL = mrPlotData_UCL_Arr.slice(((mrPlotData_UCL_Arr.length>displayNobs) ? mrPlotData_UCL_Arr.length-displayNobs : 0),mrPlotData_UCL_Arr.length);
                var mrPlotData_USD1_Arr = chartData[search]["charts"][chartIt][byValue]["MR_USD1"];
                var mrPlotData_USD1 = mrPlotData_USD1_Arr.slice(((mrPlotData_USD1_Arr.length>displayNobs) ? mrPlotData_USD1_Arr.length-displayNobs : 0),mrPlotData_USD1_Arr.length);
                var mrPlotData_USD2_Arr = chartData[search]["charts"][chartIt][byValue]["MR_USD2"];
                var mrPlotData_USD2 = mrPlotData_USD2_Arr.slice(((mrPlotData_USD2_Arr.length>displayNobs) ? mrPlotData_USD2_Arr.length-displayNobs : 0),mrPlotData_USD2_Arr.length);
                var mrPlotData_LCL_Arr = chartData[search]["charts"][chartIt][byValue]["MR_LCL"];
                var mrPlotData_LCL = mrPlotData_LCL_Arr.slice(((mrPlotData_LCL_Arr.length>displayNobs) ? mrPlotData_LCL_Arr.length-displayNobs : 0),mrPlotData_LCL_Arr.length);

                mrPlotData.unshift("mrPlotData");
                mrPlotData_CL.unshift("mrPlotData_CL");
                mrPlotData_UCL.unshift("mrPlotData_UCL");
                mrPlotData_USD1.unshift("mrPlotData_USD1");
                mrPlotData_USD2.unshift("mrPlotData_USD2");
                mrPlotData_LCL.unshift("mrPlotData_LCL");

                displayObject[search][chartIt][byValue]["mrPlotData"] = mrPlotData;
                displayObject[search][chartIt][byValue]["mrPlotData_CL"] = mrPlotData_CL;
                displayObject[search][chartIt][byValue]["mrPlotData_UCL"] = mrPlotData_UCL;
                displayObject[search][chartIt][byValue]["mrPlotData_USD1"] = mrPlotData_USD1;
                displayObject[search][chartIt][byValue]["mrPlotData_USD2"] = mrPlotData_USD2;
                displayObject[search][chartIt][byValue]["mrPlotData_LCL"] = mrPlotData_LCL;

                var runsRules_Arr = chartData[search]["charts"][chartIt][byValue]["RunsRules"];
                var runsRules = runsRules_Arr.slice(((runsRules_Arr.length>displayNobs) ? runsRules_Arr.length-displayNobs : 0),runsRules_Arr.length);

                var mrRunsRules_Arr = chartData[search]["charts"][chartIt][byValue]["MR_RunsRules"];
                var mrRunsRules = (mrRunsRules_Arr!=undefined && mrRunsRules_Arr.length>0) ? mrRunsRules_Arr.slice(((mrRunsRules_Arr.length>displayNobs) ? mrRunsRules_Arr.length-displayNobs : 0),mrRunsRules_Arr.length) : undefined;

                displayObject[search][chartIt][byValue]["runsRules"] = runsRules;
                displayObject[search][chartIt][byValue]["mrRunsRules"] = mrRunsRules;

                $("#" + outerChartDiv.replace(":","\\:")).remove();
                if (! $("#" + outerChartDiv).length) { 
                    $("#qccharts_" + search.replace(":","\\:")).append("<div id=\"" + outerChartDiv + "\" class=\"qcchart\">" + chartData[search]["charts"][chartIt][byValue]["ChartTitle"] + "</div>");
                }
                if (! $("#" + innerChartDiv).length) { 
                    $("#" + outerChartDiv.replace(":","\\:")).append("<div id=\"" + innerChartDiv + "\"></div>");
                }
                var chart = c3.generate({
                    bindto: '#' + innerChartDiv.replace(":","\\:"),
                    size: { height: 330
                            },
                    data: {
                            columns: [ plotData,plotData_CL,plotData_USD1,plotData_USD2,plotData_UCL,plotData_LSD1,plotData_LSD2,plotData_LCL ],
                            //colors: { plotData:'#084fab',plotData_CL:'#666666',plotData_UCL:'#666666',plotData_LCL:'#666666' },
                            color: function (color, d) {
                                        if (d.id == 'plotData') {
                                            try {
                                                if (runsRules[d.index].length>0) {
                                                    return '#CC0000';
                                                }
                                            } catch(e) {
                                                return '#3AB570';
                                            } 
                                            return '#3A70B5';
                                        } else {
                                            return '#9FB1C8';
                                        }
                                   },
                            regions: { 
                                        'plotData_USD1':[{'start':0, 'style':'dashed'}],
                                        'plotData_USD2':[{'start':0, 'style':'dashed'}],
                                        'plotData_LSD1':[{'start':0, 'style':'dashed'}],
                                        'plotData_LSD2':[{'start':0, 'style':'dashed'}]
                                    }
                    },
                    tooltip: {
                        show: true,
                        contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                                try {
                                    var searchVar = this["myinfo"]["searchVar"];
                                    var itemVar = this["myinfo"]["itemVar"];
                                    var byVar = this["myinfo"]["byVar"];
                                    var displayObj = this["myinfo"]["chartDisplayObj"][searchVar][itemVar][byVar];
                                    var ruleInfo = (displayObj["runsRules"][d[0].index].length>0) ?
                                           "<tr><td><nobr>Rule violation(s):</nobr></td><td style=\"color:#CC0000\">" + displayObj["runsRules"][d[0].index] + "</td></tr>" : "";
                                    var infoPrint = "";
                                    for (var infoKey in infoVar) {
                                        infoPrint = infoPrint + 
                                            "<tr><td>" + infoKey + ":</td><td>" + infoVar[infoKey][byVar][d[0].index] + "</td></tr>";
                                    }
                                    return "<div class=\"qcchart_tooltip\"><table>" + ruleInfo +
                                            "<tr><td><nobr>" + itemVar + ":</nobr></td><td>" + displayObj["plotData"][d[0].index+1] + "</td></tr>" + 
                                            "<tr><td>CL:</td><td>" + displayObj["plotData_CL"][d[0].index+1] + "</td></tr>" + 
                                            "<tr><td>UCL:</td><td>" + displayObj["plotData_UCL"][d[0].index+1] + "</td></tr>" + 
                                            "<tr><td>LCL:</td><td>" + displayObj["plotData_LCL"][d[0].index+1] + "</td></tr>" + 
                                            infoPrint + 
                                            "</table></div>";           
                                } catch (ee) {
                                    return null;           
                                }
                        }
                    },
                    legend: {
                        show: false
                    },
                    point: {
                        show: true,
                        focus: {
                            expand: {
                                enabled: true
                            },
                        },
                        select: {
                             r: function(x) {
                                if (x.id!="plotData") {
                                   return 0;
                                } else {
                                   return 4;
                                }
                             } 
                         },
                         r: function(x) {
                            if (x.id!="plotData") {
                                return 0;
                            } else {
                                return 3;
                            }
                         } 
                    },
                    axis: {
                        x: {
                                show: true,
                                tick: {  
                                    count: 12,
                                    format: function (x) { 
                                            var bindDiv = this["config"]["bindto"];
                                            var byVar = bindDiv.replace(/^#QC_SRCH_.*BYVALUE_/,"").replace(/_ITEM_.*$/,"").replace(/\\/,"");
                                            var itemVar = bindDiv.replace(/^.*_ITEM_/,"").replace(/_chart$/,"");
                                            for (var infoKey in infoVar) {
                                                if (infoKey.toLowerCase().indexOf("date")>=0) {
                                                    return (infoVar[infoKey][byVar][x]); //.toString().replace(/-/g,"");
                                                }
                                            }
                                            return '';
                                     }
                                }
                            },
                        y: {
                                label: { text: chartIt, position: 'outer-middle' }
                            } 
                    }, 
                });
                chart["internal"]["myinfo"]={};
                chart["internal"]["myinfo"]["searchVar"] = search;
                chart["internal"]["myinfo"]["itemVar"] = chartIt;
                chart["internal"]["myinfo"]["byVar"] = byValue;
                chart["internal"]["myinfo"]["chartDisplayObj"] = displayObject;
                if (! $("#" + innerMrChartDiv).length) { 
                    $("#" + outerChartDiv.replace(":","\\:")).append("<div id=\"" + innerMrChartDiv + "\"></div>");
                }
                var mrChart = c3.generate({
                    bindto: '#' + innerMrChartDiv.replace(":","\\:"),
                    size: { height: 220
                    },
                    data: {
                            columns: [ mrPlotData,mrPlotData_CL,mrPlotData_USD1,mrPlotData_USD2,mrPlotData_UCL,mrPlotData_LCL ],
                            //colors: { mrPlotData:'#084fab',mrPlotData_CL:'#666666',mrPlotData_UCL:'#666666',mrPlotData_LCL:'#666666' }
                            color: function (color, d) {
                                        if (d.id == 'mrPlotData') {
                                            return '#3A70B5';
                                        } else {
                                            return '#9FB1C8';
                                        }
                                   },
                            regions: { 
                                        'mrPlotData_USD1':[{'start':0, 'style':'dashed'}],
                                        'mrPlotData_USD2':[{'start':0, 'style':'dashed'}]
                                    }
                    },
                    tooltip: {
                        show: true,
                        contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                                try {
                                    var searchVar = this["myinfo"]["searchVar"];
                                    var itemVar = this["myinfo"]["itemVar"];
                                    var byVar = this["myinfo"]["byVar"];
                                    var displayObj = this["myinfo"]["chartDisplayObj"][searchVar][itemVar][byVar];
                                    var ruleInfo = (displayObj["mrRunsRules"] != undefined && displayObj["mrRunsRules"][d[0].index].length>0) ?
                                           "<tr><td><nobr>Rule violation(s):</nobr></td><td style=\"color:#CC0000\">" + displayObj["mrRunsRules"][d[0].index] + "</td></tr>" : "";
                                    var infoPrint = "";
                                    for (var infoKey in infoVar) {
                                        infoPrint = infoPrint + 
                                            "<tr><td>" + infoKey + ":</td><td>" + infoVar[infoKey][byVar][d[0].index] + "</td></tr>";
                                    }
                                    return "<div class=\"qcchart_tooltip\"><table>" + ruleInfo +
                                            "<tr><td><nobr>MR Value</nobr></td><td>" + displayObj["mrPlotData"][d[0].index+1] + "</td></tr>" + 
                                            "<tr><td>CL:</td><td>" + displayObj["mrPlotData_CL"][d[0].index+1] + "</td></tr>" + 
                                            "<tr><td>UCL:</td><td>" + displayObj["mrPlotData_UCL"][d[0].index+1] + "</td></tr>" + 
                                            infoPrint + 
                                            "</table></div>";           
                                } catch (ee) {
                                    return null;           
                                }
                        }
                    },
                    legend: {
                        show: false
                    },
                    point: {
                        show: true,
                        focus: {
                            expand: {
                                enabled: true
                            },
                        },
                        select: {
                             r: function(x) {
                                if (x.id!="mrPlotData") {
                                   return 0;
                                } else {
                                   return 4;
                                }
                             } 
                         },
                         r: function(x) {
                            if (x.id!="mrPlotData") {
                                return 0;
                            } else {
                                return 3;
                            }
                         } 
                    },
                    axis: {
                        x: {
                                show: true,
                                tick: {  
                                    count: 12,
                                    format: function (x) { 
                                            var bindDiv = this["config"]["bindto"];
                                            var byVar = bindDiv.replace(/^#QC_SRCH_.*BYVALUE_/,"").replace(/_ITEM_.*$/,"").replace(/\\/,"");
                                            var itemVar = bindDiv.replace(/^.*_ITEM_/,"").replace(/_mrchart$/,"");
                                            for (var infoKey in infoVar) {
                                                if (infoKey.toLowerCase().indexOf("date")>=0) {
                                                    return (infoVar[infoKey][byVar][x]); //.toString().replace(/-/g,"");
                                                }
                                            }
                                            return '';
                                     }
                                }
                            },
                        y: {
                                label: { text: 'Moving Range', position: 'outer-middle' }
                            } 
                    }, 
                });
                mrChart["internal"]["myinfo"]={};
                mrChart["internal"]["myinfo"]["searchVar"] = search;
                mrChart["internal"]["myinfo"]["itemVar"] = chartIt;
                mrChart["internal"]["myinfo"]["byVar"] = byValue;
                mrChart["internal"]["myinfo"]["chartDisplayObj"] = displayObject;
            } catch(e) {
                // Do nothing
                console.log(e);
            }

    }
            
}


