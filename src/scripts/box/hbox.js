/*  hbox.js - This is a modification of box.js by MRH to produce horizontal rather than vertical box plots */ 
(function() {
d3.hbox = function() {
  var height = 1,
      width = 1,
      duration = 0,
      domain = null,
      value = Number,
      whiskers = hboxWhiskers,
      quartiles = hboxQuartiles,
      //tickFormat = "TICK";
      tickFormat = null,
      showTickLines = false;

  // For each small multiple…
  function hbox(g) {
    g.each(function(d, i) {
      d = d.map(value).sort(d3.ascending);
      var g = d3.select(this),
          n = d.length,
          min = d[0],
          max = d[n - 1];


      //d3.svg.axis().scale(x).ticks([10]);

      // Compute quartiles. Must return exactly 3 elements.
      var quartileData = d.quartiles = quartiles(d);

      // Compute whiskers. Must return exactly 2 elements, or null.
      var whiskerIndices = whiskers && whiskers.call(this, d, i),
          whiskerData = whiskerIndices && whiskerIndices.map(function(i) { return d[i]; });

      // Compute outliers. If no whiskers are specified, all data are "outliers".
      // We compute the outliers as indices, so that we can join across transitions!
      var outlierIndices = whiskerIndices
          ? d3.range(0, whiskerIndices[0]).concat(d3.range(whiskerIndices[1] + 1, n))
          : d3.range(n);

      // Compute the new y-scale.
      var y1 = d3.scale.linear()
          .domain(domain && domain.call(this, d, i) || [min, max])
          .range([0,width]);

      // Retrieve the old y-scale, if this is an update.
      var y0 = this.__chart__ || d3.scale.linear()
          .domain([0, Infinity])
          .range(y1.range());

      // Stash the new scale.
      this.__chart__ = y1;

      // Note: the hbox, median, and hbox tick elements are fixed in number,
      // so we only have to handle enter and update. In contrast, the outliers
      // and other elements are variable, so we need to exit them! Variable
      // elements also fade in and out.

      // Update ticks.
      if (showTickLines) {
        var tick = g.selectAll("line.tick")
            .data(y1.ticks([5]));
  
        tick.enter().insert("line")
            .attr("class", "tick")
            .attr("y1", -50)
            .attr("x1", y0)
            .attr("y2", height+50)
            .attr("x2", y0)
            .style("opacity", 1e-6)
          .transition()
            .duration(duration)
            .attr("x1", y1)
            .attr("x2", y1)
            .style("opacity", 1);
  
        tick.transition()
            .duration(duration)
            .attr("x1", y1)
            .attr("x2", y1)
            .style("opacity", 1);
  
        tick.exit().transition()
            .duration(duration)
            .attr("x1", y1)
            .attr("x2", y1)
            .style("opacity", 1e-6)
            .remove();
      }

      // Update center line: the vertical line spanning the whiskers.
      var center = g.selectAll("line.center")
          .data(whiskerData ? [whiskerData] : []);

      center.enter().insert("line", "rect")
          .attr("class", "center")
          .attr("y1", height / 2)
          .attr("x1", function(d) { return y0(d[0]); })
          .attr("y2", height / 2)
          .attr("x2", function(d) { return y0(d[1]); })
          .style("opacity", 1e-6)
        .transition()
          .duration(duration)
          .style("opacity", 1)
          .attr("x1", function(d) { return y1(d[0]); })
          .attr("x2", function(d) { return y1(d[1]); });

      center.transition()
          .duration(duration)
          .style("opacity", 1)
          .attr("x1", function(d) { return y1(d[0]); })
          .attr("x2", function(d) { return y1(d[1]); });

      center.exit().transition()
          .duration(duration)
          .style("opacity", 1e-6)
          .attr("x1", function(d) { return y1(d[0]); })
          .attr("x2", function(d) { return y1(d[1]); })
          .remove();

      // Update innerquartile hbox.
      var hbox = g.selectAll("rect.hbox")
          .data([quartileData]);

      hbox.enter().append("rect")
          .attr("class", "box")
          .attr("y", 0)
          .attr("x", function(d) { return y0(d[0]); })
          .attr("height", height)
          .attr("width", function(d) { return y0(d[0]) - y0(d[0]); })
        .transition()
          .duration(duration)
          .attr("x", function(d) { return y1(d[0]); })
          .attr("width", function(d) { return y1(d[2]) - y1(d[0]); });

      hbox.transition()
          .duration(duration)
          .attr("x", function(d) { return y1(d[0]); })
          .attr("width", function(d) { return y1(d[2]) - y1(d[0]); });

      // Update median line.
      var medianLine = g.selectAll("line.median")
          .data([quartileData[1]]);

      medianLine.enter().append("line")
          .attr("class", "median")
          .attr("y1", 0)
          .attr("x1", y0)
          .attr("y2", height)
          .attr("x2", y0)
        .transition()
          .duration(duration)
          .attr("x1", y1)
          .attr("x2", y1);

      medianLine.transition()
          .duration(duration)
          .attr("x1", y1)
          .attr("x2", y1);

      // Update whiskers.
      var whisker = g.selectAll("line.whisker")
          .data(whiskerData || []);

      whisker.enter().insert("line", "circle, text")
          .attr("class", "whisker")
          .attr("y1", 0)
          .attr("x1", y0)
          .attr("y2", height)
          .attr("x2", y0)
          .style("opacity", 1e-6)
        .transition()
          .duration(duration)
          .attr("x1", y1)
          .attr("x2", y1)
          .style("opacity", 1);

      whisker.transition()
          .duration(duration)
          .attr("x1", y1)
          .attr("x2", y1)
          .style("opacity", 1);

      whisker.exit().transition()
          .duration(duration)
          .attr("x1", y1)
          .attr("x2", y1)
          .style("opacity", 1e-6)
          .remove();

      // Update outliers.
      var outlier = g.selectAll("circle.outlier")
          .data(outlierIndices, Number);

      outlier.enter().insert("circle", "text")
          .attr("class", "outlier")
          .attr("r", 5)
          .attr("cy", height / 2)
          .attr("cx", function(i) {
		 return y0(d[i]); 
		})
          .style("opacity", 1e-6)
        .transition()
          .duration(duration)
          .attr("cx", function(i) { return y1(d[i]); })
          .style("opacity", 1);

      outlier.transition()
          .duration(duration)
          .attr("cx", function(i) { return y1(d[i]); })
          .style("opacity", 1);

      outlier.exit().transition()
          .duration(duration)
          .attr("cx", function(i) { return y1(d[i]); })
          .style("opacity", 1e-6)
          .remove();

      // Compute the tick format.
      var format = tickFormat || y1.tickFormat(8);

      // Update hbox ticks.
      var hboxTick = g.selectAll("text.hbox")
          .data(quartileData);

      hboxTick.enter().append("text")
          .attr("class", "box")
          .attr("dx", ".3em")
          .attr("dy", function(d, i) { return i & 1 ? 9 : -6 })
          .attr("y", function(d, i) { return i & 1 ? height : 0 })
          .attr("x", y0)
          .attr("text-anchor", function(d, i) { return i & 1 ? "start" : "end"; })
          .text(format)
        .transition()
          .duration(duration)
          .attr("x", y1);

      hboxTick.transition()
          .duration(duration)
          .text(format)
          .attr("x", y1);

      // Update whisker ticks. These are handled separately from the hbox
      // ticks because they may or may not exist, and we want don't want
      // to join hbox ticks pre-transition with whisker ticks post-.
      var whiskerTick = g.selectAll("text.whisker")
          .data(whiskerData || []);

      whiskerTick.enter().append("text")
          .attr("class", "whisker")
          .attr("dx", ".3em")
          .attr("dy", 6)
          .attr("y", height)
          .attr("x", y0)
          .text(format)
          .style("opacity", 1e-6)
        .transition()
          .duration(duration)
          .attr("x", y1)
          .style("opacity", 1);

      whiskerTick.transition()
          .duration(duration)
          .text(format)
          .attr("x", y1)
          .style("opacity", 1);

      whiskerTick.exit().transition()
          .duration(duration)
          .attr("x", y1)
          .style("opacity", 1e-6)
          .remove();
    });
    d3.timer.flush();
  }

  hbox.height = function(x) {
    if (!arguments.length) return height;
    height = x;
    return hbox;
  };

  hbox.width = function(x) {
    if (!arguments.length) return width;
    width = x;
    return hbox;
  };

  hbox.tickFormat = function(x) {
    if (!arguments.length) return tickFormat;
    tickFormat = x;
    return hbox;
  };

  hbox.duration = function(x) {
    if (!arguments.length) return duration;
    duration = x;
    return hbox;
  };

  hbox.domain = function(x) {
    if (!arguments.length) return domain;
    domain = x == null ? x : d3.functor(x);
    return hbox;
  };

  hbox.value = function(x) {
    if (!arguments.length) return value;
    value = x;
    return hbox;
  };

  hbox.whiskers = function(x) {
    if (!arguments.length) return whiskers;
    whiskers = x;
    return hbox;
  };

  hbox.showTickLines = function(x) {
    if (!arguments.length) return showTickLines;
    showTickLines = x;
    return hbox;
  };

  hbox.quartiles = function(x) {
    if (!arguments.length) return quartiles;
    quartiles = x;
    return hbox;
  };

  return hbox;
};

function hboxWhiskers(d) {
  return [0, d.length - 1];
}

function hboxQuartiles(d) {
  return [
    d3.quantile(d, .25),
    d3.quantile(d, .5),
    d3.quantile(d, .75)
  ];
}

})();

(function() {
// Inspired by http://informationandvisualization.de/blog/hbox-plot
d3.hboxhdr = function() {
  var height = 1,
      width = 1,
      duration = 0,
      domain = null,
      value = Number,
      //tickFormat = "TICK";
      tickFormat = null,
      showTickLines = true;

  // For each small multiple…
  function hboxhdr(g) {
    g.each(function(d, i) {
      d = d.map(value).sort(d3.ascending);
      var g = d3.select(this),
          n = d.length,
          min = d[0],
          max = d[n - 1];

      // Compute the new y-scale.
      var y1 = d3.scale.linear()
          .domain(domain && domain.call(this, d, i) || [min, max])
          .range([0,width]);

      // Retrieve the old y-scale, if this is an update.
      var y0 = this.__chart__ || d3.scale.linear()
          .domain([0, Infinity])
          .range(y1.range());

      // Stash the new scale.
      this.__chart__ = y1;

      // Note: the hbox, median, and hbox tick elements are fixed in number,
      // so we only have to handle enter and update. In contrast, the outliers
      // and other elements are variable, so we need to exit them! Variable
      // elements also fade in and out.

      // Update ticks.
      //if (showTickLines) {
      var tick = g.selectAll("line.tick")
          .data(y1.ticks([5]));

      var format = tickFormat || y1.tickFormat(8);
      tick.enter().insert("text")
          .attr("class", "box")
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("y", 0)
          .attr("x", y0)
          .attr("text-anchor", "middle")
          .text(format)
        .transition()
          .duration(duration)
          .attr("x", y1);
    });
    d3.timer.flush();
  }

  hboxhdr.height = function(x) {
    if (!arguments.length) return height;
    height = x;
    return hboxhdr;
  };

  hboxhdr.width = function(x) {
    if (!arguments.length) return width;
    width = x;
    return hboxhdr;
  };

  hboxhdr.tickFormat = function(x) {
    if (!arguments.length) return tickFormat;
    tickFormat = x;
    return hboxhdr;
  };

  hboxhdr.duration = function(x) {
    if (!arguments.length) return duration;
    duration = x;
    return hboxhdr;
  };

  hboxhdr.domain = function(x) {
    if (!arguments.length) return domain;
    domain = x == null ? x : d3.functor(x);
    return hboxhdr;
  };

  hboxhdr.value = function(x) {
    if (!arguments.length) return value;
    value = x;
    return hboxhdr;
  };

  hboxhdr.showTickLines = function(x) {
    if (!arguments.length) return showTickLines;
    showTickLines = x;
    return hboxhdr;
  };

  return hboxhdr;
};

})();
